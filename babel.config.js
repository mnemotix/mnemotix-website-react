module.exports = function(api) {
  let isDev = process.env.NODE_ENV !== 'production';

  api.cache(false);

  const presets = [
    [
      '@babel/preset-env',
      {
        useBuiltIns: 'usage',
        corejs: 3,
        targets: {
          ie:"11"
        },
        shippedProposals: true
      }
    ],
    '@babel/react'
  ];

  const plugins = [
    ['@babel/proposal-optional-chaining'],
    [
      'babel-plugin-import',
      {
        libraryName: '@material-ui/core',
        // Use "'libraryDirectory': ''," if your bundler does not support ES modules
        libraryDirectory: 'esm',
        camel2DashComponentName: false
      },
      'core'
    ],
    [
      'babel-plugin-import',
      {
        libraryName: '@material-ui/icons',
        // Use "'libraryDirectory': ''," if your bundler does not support ES modules
        libraryDirectory: 'esm',
        camel2DashComponentName: false
      },
      'icons'
    ]
  ];

  if (isDev) {
    plugins.push('@babel/plugin-transform-react-jsx-source');
    //plugins.push('@babel/transform-react-jsx-source');
  }

  return {
    presets,
    plugins
  };
};
