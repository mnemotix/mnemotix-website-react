import globalStyles from '../globalStyles';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';

const spacing = 5;
const useStyles = makeStyles((theme) => ({
  padding: {
    padding: spacing * 4, // il faut ajouter un paddig a cause de la marge négative ajoutée spacing qui crée un scroll horizontal
  },
}));

/**
 * pour appliquer le meme style à tout les grid que l'on utilise
 */
export default function useGridClasses() {
  const gS = globalStyles();
  const classes = useStyles();

  return {
    grid: {
      direction: 'row',
      justifyContent: 'center',
      alignItems: 'flex-start',
      spacing,
    },
    parentClassName: classes.padding,
    className: clsx(gS.flexStart, gS.flexDirectionCol, gS.paddingB50),
  };
}
