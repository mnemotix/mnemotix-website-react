import { useTheme } from '@material-ui/core/styles';
import useMediaQuery from '@material-ui/core/useMediaQuery';

export default function useResponsive() {
  const theme = useTheme();

  const isMobile = useMediaQuery(theme.breakpoints.down('sm'));
  return {
    isMobile,
    isDesktop: !isMobile,
  };
}

/* 
export default function useResponsive() {
  const theme = useTheme();
  const isTablet = useMediaQuery(theme.breakpoints.between('xs', 'sm'));
  const isMobile = useMediaQuery(theme.breakpoints.down('xs'));
  return {
    isMobile,
    isTablet,
    isDesktop: !isMobile && !isTablet,
  };
}

*/
