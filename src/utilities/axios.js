import * as axios from 'axios';

let instance = axios.create();

instance.defaults.baseURL = window.location.origin + '/api';
instance.defaults.timeout = 15000;

const postConfig = {
  method: 'POST',
  headers: { Accept: 'application/json', 'Content-Type': 'application/json' },
};

export { instance, postConfig };
