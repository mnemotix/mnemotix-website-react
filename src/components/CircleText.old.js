import React from 'react';
import Config from '../Config';
import styled from 'styled-components';

const StyleDiv = styled('div')`
  .circletext {
    width: min(500px, 40vw);
    height: min(500px, 40vw);
    position: relative;
    margin: auto;
    border: 1em solid ${Config.colors.orangeDarker};
    background: ${Config.colors.creme};
    border-radius: 50%;
  }
  @media (max-width: 720px) {
    .circletext {
      width: 280px;
      height: 280px;
      width: 80vw;
      height: 80vw;
    }
  }

  @media (max-width: 320px) {
    .circletext {
      width: 80vw;
      height: 80vw;
    }
  }

  .circletext .text {
    width: 100%;
    height: 100%;
    position: relative;
    margin: 0;
    color: ${Config.colors.darkBlue};
  }

  .text p {
    height: 100%;
    padding: 0;
    margin: auto;
    font-size: calc(10px + 1.5vmin) !important;
    text-align: center;
    font-style: italic;
    text-shadow: 0.5px 0.5px 1px rgba(0, 0, 0, 0.3);
  }

  .text::before {
    content: '';
    width: 40%;
    height: 100%;
    float: left;
    shape-outside: polygon(0 0, 98% 0, 50% 6%, 23.4% 17.3%, 6% 32.6%, 0 50%, 6% 65.6%, 23.4% 82.7%, 50% 94%, 98% 100%, 0 100%);
    shape-margin: 7%;
  }
  .text p::before {
    content: '';
    width: 40%;
    height: 100%;
    float: right;
    shape-outside: polygon(2% 0%, 100% 0%, 100% 100%, 2% 100%, 50% 94%, 76.6% 82.7%, 94% 65.6%, 100% 50%, 94% 32.6%, 76.6% 17.3%, 50% 6%);
    shape-margin: 7%;
  }
  .circletext::before {
    font-size: 270px;
    height: 82px;

    line-height: 1;
    position: absolute;
    top: -48px;
    left: 0;
    z-index: 1;
    font-family: sans-serif, serif;
    color: #ccc;
    opacity: 0.9;
  }
`;

/**
 *  Permet d'afficher un contenu avec flexflow wrap,
 * si on par exemple plein de div, cela les affiche en ligne sur autant de ligne que necessaire
 *
 * utilisé par exemple dans la navbar ou le footer
 * @param {*} param0
 */
export default function CircleText({ children }) {
  return (
    <StyleDiv>
      <div className="circletext">
        <div className="text">
          <p className="verticalAlign">{children}</p>
        </div>
      </div>
    </StyleDiv>
  );
}
