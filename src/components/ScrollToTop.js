import React from 'react';

import { makeStyles } from '@material-ui/core/styles';
import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import Fab from '@material-ui/core/Fab';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import Zoom from '@material-ui/core/Zoom';
import Config from '../Config';

const useStyles = makeStyles((theme) => ({
  root: {
    position: 'fixed',
    bottom: theme.spacing(2),
    right: theme.spacing(2),
  },
  fab: {
    color: Config.colors.orange,
    backgroundColor: Config.colors.darkBlue,
    '&:hover': {
      color: Config.colors.darkBlue,
      backgroundColor: Config.colors.orangeDarker,
    },
  },
}));

/**
 * Scroll to Top
 */
function ScrollToTop({ id }) {
  const classes = useStyles();

  const trigger = useScrollTrigger({
    disableHysteresis: true,
    threshold: 100,
  });

  const handleClick = (event) => {
    const anchor = document.querySelector('#' + id);
    if (anchor) {
      anchor.scrollIntoView({ block: 'start', behavior: 'smooth', inline: 'nearest' });
    }
  };

  return (
    <Zoom in={trigger}>
      <div onClick={handleClick} role="presentation" className={classes.root}>
        <Fab className={classes.fab} size="small" aria-label="scroll back to top">
          <KeyboardArrowUpIcon />
        </Fab>
      </div>
    </Zoom>
  );
}

export default ScrollToTop;
