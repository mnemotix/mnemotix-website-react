import React from 'react';
import globalStyles from '../globalStyles';
import clsx from 'clsx';
import Container from '@material-ui/core/Container';
/**
 * permet de mettre une largeur max et de centrer du contenu
 *
 */
export default function MaxWidthCentered({ size = 'lg', children, ...props }) {
  const gS = globalStyles();

  return (
    <Container maxWidth={size} {...props} style={{minHeight:'min(600px,100vh) !important '}}>
      <div className={clsx(gS.flexCenter, gS.flexDirectionCol)}>{children}</div>
    </Container>
  );
}
