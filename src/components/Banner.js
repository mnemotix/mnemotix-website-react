import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import MaxWidthCentered from './MaxWidthCentered'; 
import Config from '../Config';

const useStyles = makeStyles((theme) => ({
  bannerRoot: {
    height: '100%',    
    width: '100%',
    // position: 'relative',
    backgroundColor: Config.colors.darkBlue,
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    display: 'flex',
    flexGrow: 1,
  },
  marginAuto: {
    margin: 'auto',
  },
}));

/**
 * display a background 100% width and a content
 * used for TopSlider and project's page
 * @param {*}
 */

export default function Banner({ children, style = {}, ...props }) {
  const classes = useStyles(); 
  return (
    <div className={classes.bannerRoot} style={style}>
      <MaxWidthCentered className={classes.marginAuto}>{children}</MaxWidthCentered> 
    </div>
  );
}
