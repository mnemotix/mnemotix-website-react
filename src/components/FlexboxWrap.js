import React from 'react';

import styled from 'styled-components';

const StyleFlexboxWrap = styled('div')`
  list-style: none;
  -ms-box-orient: horizontal;
  display: -webkit-box;
  display: -moz-box;
  display: -ms-flexbox;
  display: -moz-flex;
  display: -webkit-flex;
  display: flex;
  align-items: center;
  justify-content: flex-start;

  -webkit-flex-flow: wrap row;
  flex-flow: wrap row;
`;

/**
 *  Permet d'afficher un contenu avec flexflow wrap,
 * si on par exemple plein de div, cela les affiche en ligne sur autant de ligne que necessaire
 *
 * utilisé par exemple dans la navbar ou le footer
 * @param {*} param0
 */
export default function FlexboxWrap({ children }) {
  return <StyleFlexboxWrap>{children}</StyleFlexboxWrap>;
}
