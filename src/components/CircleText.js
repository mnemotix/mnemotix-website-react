import React from 'react';
import Config from '../Config';
import styled from 'styled-components';


/*
 .circle-container {
    @media (min-width: 960px) and (max-width: 1199px) {
      width: min(400px, 35vw);
      height: min(400px, 35vw);
    }
  }
*/


const StyleDivR = styled('div')`
  .circle-container {
    position: relative;
    margin: auto;
    border-radius: 50%;

    width: min(400px, 50vw);
    height: min(400px, 50vw);
   
    @media (min-width: 721px) and (max-width: 959px) {
      width: 40vw;
      height: 40vw;
    }
    @media (max-width: 720px) {
      width: 65vw;
      height: 65vw;
    }
  }

  .color-a {
    border: 1.5em solid ${Config.colors.orangeDarker};
    background: ${Config.colors.creme};
  }
  .color-b {
    border: 1.5em solid ${Config.colors.creme};
    background: ${Config.colors.orangeDarker};
  }

  .circle {
    border-radius: 50%;
    color: #fff;
    display: block;
    height: 0;
    overflow: hidden;
    padding-top: 85%;
    position: relative;
    width: 100%;
  }

  .circle-content {
    align-items: center;
    display: flex;
    height: 100%;
    hyphens: auto;
    justify-content: center;
    left: 50%;
    padding: 1em;
    position: absolute;
    transform: translate(-50%, -50%);
    top: 50%;
    width: 90%;
    z-index: 99;
    color: ${Config.colors.darkBlue};
    text-align: center;
    font-style: italic;
    text-shadow: 0.5px 0.5px 1px rgba(0, 0, 0, 0.3);
  }
`;

/**
 *  Permet d'afficher un contenu avec flexflow wrap,
 * si on par exemple plein de div, cela les affiche en ligne sur autant de ligne que necessaire
 *
 * utilisé par exemple dans la navbar ou le footer
 * @param {*} children text to display
 * @param {bool} secondColor : display circle with another color
 */
export default function CircleText({ children, secondColor = false }) {
  const color = 'circle-container ' + (secondColor ? 'color-b' : 'color-a');
  return (
    <StyleDivR>
      <div className={color}>
        <div className="circle">
          <p className="circle-content">{children}</p>
        </div>
      </div>
    </StyleDivR>
  );
}
