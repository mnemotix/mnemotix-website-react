import React from 'react';

import { Grid } from '@material-ui/core';
import { ShapeContainer, Dot, Edge } from './index';

export default function Graph2(props) {
  return (
    <ShapeContainer>
      <Grid container direction="row" justifyContent="center" alignItems="center">
        <Grid item xs={12}>
          <Edge style={{ top: '125px', left: '305px', transform: 'rotate(103deg)', width: '1px' }} height={93} id={'graph3-e1'} />
          <Dot noAnimation  id={'graph3-2'} color="blue" styles={[{ top: '90px', left: '210px', transform: 'scale(1.5)' }, { transform: 'scale(0.5)' }]}  />
          <Edge style={{ top: '125px', left: '305px', transform: 'rotate(152deg)', width: '1px' }} height={96} id={'graph3-e3'} />
          <Dot noAnimation 
            id={'graph3-4'}
            color="yellow"
            styles={[{ top: '30px', left: '245px', transform: 'scale(0.85)' }, { transform: 'scale(0.25)' }]}
          />
          <Edge style={{ top: '125px', left: '305px', transform: 'rotate(212deg)', width: '1px' }} height={93} id={'graph3-e5'} />
          <Dot noAnimation 
            id={'graph3-6'}
            color="orange"
            styles={[{ top: '18px', left: '342px', transform: 'scale(0.9)' }, { transform: 'scale(0.5)' }]}
          />
          <Edge style={{ top: '125px', left: '305px', transform: 'rotate(270deg)', width: '1px' }} height={118} id={'graph3-e7'} />
          <Dot noAnimation 
            id={'graph3-8'}
            color="yellow"
            styles={[{ top: '108px', left: '413px', transform: 'scale(0.8)' }, { transform: 'scale(0.5)' }]}
          />
          <Edge style={{ top: '125px', left: '305px', transform: 'rotate(318deg)', width: '1px' }} height={135} id={'graph3-e9'} />
          <Dot noAnimation 
            id={'graph3-10'}
            color="blue"
            styles={[{ top: '220px', left: '390px', transform: 'scale(0.9)' }, { transform: 'scale(0.5)' }]}
          />
          <Edge
            style={{ top: '125px', left: '305px', transform: 'rotate(356deg)', height: '0px', width: '1px' }}
            height={135}
            id={'graph3-e11'}
          />
          <Dot noAnimation 
            id={'graph3-12'}
            color="orange"
            styles={[{ top: '260px', left: '300px', transform: 'scale(1)' }, { transform: 'scale(0.5)' }]}
          />
          <Edge
            style={{ top: '125px', left: '305px', transform: 'rotate(58deg)', width: '1px' }}
            height={130}
            id={'graph3-e13'}
            styles={[
              { top: '125px', left: '305px', transform: 'rotate(58deg)', height: '0px', width: '1px' },
              { height: '30px' },
              { height: '60px' },
              { height: '100px' },
              { height: '130px' },
            ]}
          />
          <Edge style={{ top: '195px', left: '195px', transform: 'rotate(270deg)', width: '1px' }} height={67} id={'graph3-e14'} />
          <Dot noAnimation  id={'graph3-15'} color="blue" styles={[{ top: '178px', left: '257px', transform: 'scale(0.75)' }, { transform: 'scale(0.5)' }]} />
          <Edge style={{ top: '195px', left: '195px', transform: 'rotate(369deg)', width: '1px' }} height={71} id={'graph3-e16'} />
          <Dot noAnimation 
            id={'graph3-17'}
            color="yellow"
            styles={[{ top: '257px', left: '166px', transform: 'scale(0.65)' }, { transform: 'scale(0.5)' }]}
          />
          <Edge
            style={{ top: '195px', left: '195px', transform: 'rotate(70deg)', height: '0px', width: '1px' }}
            height={80}
            id={'graph3-e18'}
          />
          <Dot noAnimation 
            id={'graph3-19'}
            color="blue"
            styles={[{ top: '210px', left: '98px', transform: 'scale(0.65)' }, { transform: 'scale(0.5)' }]}
          />
          <Edge
            style={{ top: '195px', left: '195px', transform: 'rotate(141deg)', height: '0px', width: '0px' }}
            height={80}
            id={'graph3-e20'}
          />
          <Edge
            style={{ top: '135px', left: '146px', transform: 'rotate(112deg)', height: '0px', width: '1px' }}
            height={110}
            id={'graph3-e21'}
          />
          <Dot noAnimation  id={'graph3-22'} color="orange" styles={[{ top: '74px', left: '22px', transform: 'scale(0.75)' }, { transform: 'scale(0.5)' }]} />
          <Edge
            style={{ top: '135px', left: '146px', transform: 'rotate(174deg)', height: '0px', width: '1px' }}
            height={90}
            id={'graph3-23'}
          />
          <Dot noAnimation 
            id={'graph3-24'}
            color="yellow"
            styles={[{ top: '35px', left: '120px', transform: 'scale(0.75)' }, { transform: 'scale(0.5)' }]}
          />
          <Edge
            style={{ top: '135px', left: '146px', transform: 'rotate(77deg)', height: '0px', width: '1px' }}
            height={85}
            id={'graph3-e25'}
          />
          <Edge
            style={{ top: '190px', left: '190px', transform: 'rotate(141deg)', height: '0px', width: '1px' }}
            height={72}
            id={'graph3-e30'}
          />
          <Dot noAnimation 
            id={'graph3-26'}
            color="yellow"
            styles={[{ top: '139px', left: '60px', transform: 'scale(0.75)' }, { transform: 'scale(0.5)' }]}
          />
          <Dot noAnimation  id={'graph3-27'} color="blue" styles={[{ top: '116px', left: '125px', transform: 'scale(1.25)' }, { transform: 'scale(0.5)' }]} />

          <Dot noAnimation 
            id={'graph3-28'}
            color="yellow"
            styles={[{ top: '180px', left: '175px', transform: 'scale(1)' }, { transform: 'scale(0.5)' }]}
          />
          <Dot noAnimation 
            id={'graph3-29'}
            color="yellow"
            styles={[{ top: '113px', left: '288px', transform: 'scale(1)' }, { transform: 'scale(0.6)' }]}
          />
        </Grid>
      </Grid>
    </ShapeContainer>
  );
}
/* <ShapeContainer>
      <Grid container direction="row" justifyContent="center" alignItems="center">
        <Grid item xs={12}>
          <Edge 
            id={'graph3-1'}
            styles={[
              { top: '125px', left: '305px', transform: 'rotate(103deg)', height: '93px', width: '1px' },
              { height: '0px' },
              { height: '93px' },
              {},
              { height: '0px' },
            ]}
          />
          <Dot noAnimation 
            id={'graph3-2'}
            color="blue"
            styles={[
              { top: '100px', left: '210px', transform: 'scale(3)' },
              { top: '120px', left: '300px', transform: 'scale(2)' },
              { top: '100px', left: '210px', transform: 'scale(3)' },

              { top: '120px', left: '300px', transform: 'scale(2)' },
            ]}
          />
          <Edge
            id={'graph3-3'}
            styles={[
              { top: '125px', left: '305px', transform: 'rotate(152deg)', height: '96px', width: '1px' },
              { height: '0px' },
              { height: '96px' },
              {},
              { height: '0px' },
            ]}
          />
          <Dot noAnimation 
            id={'graph3-4'}
            color="yellow"
            styles={[
              { top: '30px', left: '250px', transform: 'scale(3.5)' },
              { top: '120px', left: '300px', transform: 'scale(2)' },
              { top: '30px', left: '250px', transform: 'scale(3.5)' },
              {},
              { top: '120px', left: '300px', transform: 'scale(2)' },
            ]}
          />
          <Edge
            id={'graph3-5'}
            styles={[
              { top: '125px', left: '305px', transform: 'rotate(212deg)', height: '93px', width: '1px' },
              { height: '0px' },
              { height: '93px' },
              {},
              { height: '0px' },
            ]}
          />
          <Dot noAnimation 
            id={'graph3-6'}
            color="orange"
            styles={[
              { top: '40px', left: '350px', transform: 'scale(3.5)' },
              { top: '120px', left: '300px', transform: 'scale(2)' },
              { top: '40px', left: '350px', transform: 'scale(3.5)' },
              {},
              { top: '120px', left: '300px', transform: 'scale(2)' },
            ]}
          />
          <Edge
            id={'graph3-7'}
            styles={[
              { top: '125px', left: '305px', transform: 'rotate(270deg)', height: '118px', width: '1px' },
              { height: '0px' },
              { height: '118px' },
              {},
              { height: '0px' },
            ]}
          />
          <Dot noAnimation 
            id={'graph3-8'}
            color="yellow"
            styles={[
              { top: '120px', left: '420px', transform: 'scale(3)' },
              { top: '120px', left: '300px', transform: 'scale(2)' },
              { top: '120px', left: '420px', transform: 'scale(3)' },
              {},
              { top: '120px', left: '300px', transform: 'scale(2)' },
            ]}
          />
          <Edge
            id={'graph3-9'}
            styles={[
              { top: '125px', left: '305px', transform: 'rotate(318deg)', height: '135px', width: '1px' },
              { height: '0px' },
              { height: '135px' },
              {},
              { height: '0px' },
            ]}
          />
          <Dot noAnimation 
            id={'graph3-10'}
            color="blue"
            styles={[
              { top: '220px', left: '390px', transform: 'scale(3.5)' },
              { top: '120px', left: '300px', transform: 'scale(2)' },
              { top: '220px', left: '390px', transform: 'scale(3.5)' },
              {},
              { top: '120px', left: '300px', transform: 'scale(2)' },
            ]}
          />
          <Edge
            id={'graph3-11'}
            styles={[
              { top: '125px', left: '305px', transform: 'rotate(356deg)', height: '135px', width: '1px' },
              { height: '0px' },
              { height: '135px' },
              {},
              { height: '0px' },
            ]}
          />
          <Dot noAnimation 
            id={'graph3-12'}
            color="orange"
            styles={[
              { top: '260px', left: '310px', transform: 'scale(4)' },
              { top: '120px', left: '300px', transform: 'scale(2)' },
              { top: '260px', left: '310px', transform: 'scale(4)' },
              {},
              { top: '120px', left: '300px', transform: 'scale(2)' },
            ]}
          />
          <Edge
            id={'graph3-13'}
            styles={[
              { top: '125px', left: '305px', transform: 'rotate(58deg)', height: '130px', width: '1px' },
              { height: '0px' },
              { height: '130px' },
              {},
              { height: '0px' },
            ]}
          />
          <Edge
            id={'graph3-14'}
            styles={[
              { top: '195px', left: '195px', transform: 'rotate(270deg)', height: '67px', width: '1px' },
              { height: '0px' },
              { height: '67px' },
              {},
              { height: '0px' },
            ]}
          />
          <Dot noAnimation 
            id={'graph3-15'}
            color="blue"
            styles={[
              { top: '190px', left: '260px', transform: 'scale(3)' },
              { top: '190px', left: '190px', transform: 'scale(2)' },
              { top: '190px', left: '260px', transform: 'scale(3)' },
              {},
              { top: '190px', left: '190px', transform: 'scale(2)' },
            ]}
          />
          <Edge
            id={'graph3-16'}
            styles={[
              { top: '195px', left: '195px', transform: 'rotate(369deg)', height: '71px', width: '1px' },
              { height: '0px' },
              { height: '71px' },
              {},
              { height: '0px' },
            ]}
          />
          <Dot noAnimation 
            id={'graph3-17'}
            color="yellow"
            styles={[
              { top: '260px', left: '180px', transform: 'scale(2.5)' },
              { top: '190px', left: '190px', transform: 'scale(2)' },
              { top: '260px', left: '180px', transform: 'scale(2.5)' },
              {},
              { top: '190px', left: '190px', transform: 'scale(2)' },
            ]}
          />
          <Edge
            id={'graph3-18'}
            styles={[
              { top: '195px', left: '195px', transform: 'rotate(70deg)', height: '80px', width: '1px' },
              { height: '0px' },
              { height: '80px' },
              {},
              { height: '0px' },
            ]}
          />
          <Dot noAnimation 
            id={'graph3-19'}
            color="blue"
            styles={[
              { top: '220px', left: '110px', transform: 'scale(2.5)' },
              { top: '190px', left: '190px', transform: 'scale(2)' },
              { top: '220px', left: '110px', transform: 'scale(2.5)' },
              {},
              { top: '190px', left: '190px', transform: 'scale(2)' },
            ]}
          />
          <Edge
            id={'graph3-20'}
            styles={[
              { top: '195px', left: '195px', transform: 'rotate(141deg)', height: '0px', width: '0px' },
              { height: '0px' },
              { height: '80px', width: '1px' },
              {},
              { height: '0px' },
            ]}
          />
          <Edge
            id={'graph3-21'}
            styles={[
              { top: '135px', left: '146px', transform: 'rotate(112deg)', height: '110px', width: '1px' },
              { height: '0px' },
              { height: '110px' },
              {},
              { height: '0px' },
            ]}
          />
          <Dot noAnimation 
            id={'graph3-22'}
            color="orange"
            styles={[
              { top: '90px', left: '40px', transform: 'scale(3)' },
              { top: '130px', left: '140px', transform: 'scale(2)' },
              { top: '90px', left: '40px', transform: 'scale(3)' },
              {},
              { top: '130px', left: '140px', transform: 'scale(2)' },
            ]}
          />
          <Edge
            id={'graph3-23'}
            styles={[
              { top: '135px', left: '146px', transform: 'rotate(174deg)', height: '90px', width: '1px' },
              { height: '0px' },
              { height: '90px' },
              {},
              { height: '0px' },
            ]}
          />
          <Dot noAnimation 
            id={'graph3-24'}
            color="yellow"
            styles={[
              { top: '40px', left: '130px', transform: 'scale(3)' },
              { top: '130px', left: '140px', transform: 'scale(2)' },
              { top: '40px', left: '130px', transform: 'scale(3)' },
              {},
              { top: '130px', left: '140px', transform: 'scale(2)' },
            ]}
          />
          <Edge
            id={'graph3-25'}
            styles={[
              { top: '135px', left: '146px', transform: 'rotate(77deg)', height: '85px', width: '1px' },
              { height: '0px' },
              { height: '85px' },
              {},
              { height: '0px' },
            ]}
          />
          <Dot noAnimation 
            id={'graph3-26'}
            color="yellow"
            styles={[
              { top: '150px', left: '60px', transform: 'scale(3)' },
              { top: '130px', left: '140px', transform: 'scale(2)' },
              { top: '150px', left: '60px', transform: 'scale(3)' },
              {},
              { top: '130px', left: '140px', transform: 'scale(2)' },
            ]}
          />
          <Dot noAnimation 
            id={'graph3-27'}
            color="blue"
            styles={[
              { top: '130px', left: '140px', transform: 'scale(5)' },
              { transform: 'scale(2)' },
              { transform: 'scale(5)' },
              {},
              { transform: 'scale(2)' },
            ]}
          />
          <Dot noAnimation 
            id={'graph3-28'}
            color="yellow"
            styles={[
              { top: '190px', left: '190px', transform: 'scale(4)' },
              { transform: 'scale(2)' },
              { transform: 'scale(4)' },
              {},
              { transform: 'scale(2)' },
            ]}
          />
          <Dot noAnimation 
            id={'graph3-29'}
            color="yellow"
            styles={[
              { top: '120px', left: '300px', transform: 'scale(4)' },
              { transform: 'scale(2)' },
              { transform: 'scale(6)' },
              {},
              { transform: 'scale(2.5)' },
            ]}
          />
        </Grid>
      </Grid>
    </ShapeContainer>*/
