import React from 'react';

import { Dot, ShapeContainer } from './index';

const _blue = 'blue';
const _orange = 'orange';
const _yellow = 'yellow';

function Graph1(props) {
  return (
    <ShapeContainer>
      <Dot
        id={'graph1-1'}
        color={_blue}
        styles={[        
          { top: '95px', left: '150px', transform: 'scale(1)' },
          { top: '240px', left: '50px', transform: 'scale(2)' },
        ]}
      />
      <Dot
        id={'graph1-2'}
        color={_blue}
        styles={[
          { top: '135px', left: '215px', transform: 'scale(0.5)' },
          { top: '110px', left: '140px', transform: 'scale(1)' },
        ]}
      />
      <Dot
        id={'graph1-3'}
        color={_orange}
        styles={[
          { top: '195px', left: '180px', transform: 'scale(0.25)' },
          { top: '135px', left: '210px', transform: 'scale(0.75)' },
        ]}
      />
      <Dot
        id={'graph1-4'}
        color={_orange}
        styles={[
          { top: '155px', left: '165px', transform: 'scale(0.5)' }, 
          { top: '35px', left: '350px', transform: 'scale(1)' },
        ]}
      />
      <Dot
        id={'graph1-5'}
        color={_yellow}
        styles={[
          { top: '160px', left: '260px', transform: 'scale(0.5)' },
          { top: '210px', left: '400px', transform: 'scale(1.25)' },
        ]}
      />
      <Dot
        id={'graph1-6'}
        color={_orange}
        styles={[
          { top: '80px', left: '130px', transform: 'scale(0.5)' },
          { top: '50px', left: '110px', transform: 'scale(1)' },
        ]}
      />
      <Dot
        id={'graph1-7'}
        color={_blue}
        styles={[
          { top: '60px', left: '230px', transform: 'scale(0.25)' },
          { top: '165px', left: '125px', transform: 'scale(1.25)' },
        ]}
      />
      <Dot
        id={'graph1-8'}
        color={_blue}
        styles={[
          { top: '150px', left: '220px', transform: 'scale(0.25)' },
          { top: '220px', left: '200px', transform: 'scale(0.75)' },
        ]}
      />
      <Dot
        id={'graph1-9'}
        color={_orange}
        styles={[
          { top: '177px', left: '198px', transform: 'scale(0.25)' },
          { top: '100px', left: '310px', transform: 'scale(0.75)' },
        ]}
      />
      <Dot
        id={'graph1-10'}
        color={_orange}
        styles={[
          { top: '110px', left: '150px', transform: 'scale(0.25)' },
          { top: '160px', left: '330px', transform: 'scale(0.75)' },
        ]}
      />
      <Dot
        id={'graph1-11'}
        color={_yellow}
        styles={[
          { top: '140px', left: '235px', transform: 'scale(0.5)' },
          { top: '250px', left: '320px', transform: 'scale(1)' },
        ]}
      />
      <Dot
        id={'graph1-12'}
        color={_yellow}
        styles={[
          { top: '100px', left: '270px', transform: 'scale(0.25)' },
          { top: '110px', left: '430px', transform: 'scale(1)' },
        ]}
      />

      <Dot
        id={'graph1-13'}
        color={_blue}
        styles={[
          { top: '155px', left: '210px', transform: 'scale(0.25)' },
          { top: '120px', left: '50px', transform: 'scale(0.75)' },
        ]}
      />
      <Dot
        id={'graph1-14'}
        color={_orange}
        styles={[
          { top: '195px', left: '190px', transform: 'scale(0.75)' },
          { top: '60px', left: '210px', transform: 'scale(1.5)' },
        ]}
      />
      <Dot
        id={'graph1-15'}
        color={_yellow}
        styles={[
          { top: '145px', left: '240px', transform: 'scale(0.5)' },
          { top: '190px', left: '270px', transform: 'scale(1)' },
        ]}
      />
    </ShapeContainer>
  );
}

export default Graph1;
