import React from 'react';
import clsx from 'clsx';
import { Grid } from '@material-ui/core';
import { ShapeContainer, GaugeMeter, Edge } from './index';

import globalStyles from '../../globalStyles';

let showLeftOnly = true;

export default function Graph3(props) {
  const gS = globalStyles();
  return (
    <Grid container direction="row" justifyContent="center" alignItems="center">
      <Grid item xs={showLeftOnly ? 12 : 6} className={clsx(showLeftOnly && gS.flexCenter)}>
        <ShapeContainer>
          <Edge
            id={'graph4-1'}
            color="blue"
            styles={[
              { top: '237px', left: '60px', transform: 'rotate(180deg)', height: '44px' },
              { height: '2px' },
              { height: '44px' },
              {},
              { height: '2px' },
            ]}
          />
          <Edge
            id={'graph4-2'}
            color="blue"
            styles={[
              { top: '237px', left: '100px', transform: 'rotate(180deg)', height: '112px' },
              { height: '2px' },
              { height: '112px' },
              {},
              { height: '2px' },
            ]}
          />
          <Edge
            id={'graph4-3'}
            color="blue"
            styles={[
              { top: '237px', left: '140px', transform: 'rotate(180deg)', height: '88px' },
              { height: '2px' },
              { height: '88px' },
              {},
              { height: '2px' },
            ]}
          />
          <Edge
            id={'graph4-4'}
            color="blue"
            styles={[
              { top: '237px', left: '180px', transform: 'rotate(180deg)', height: '196px' },
              { height: '2px' },
              { height: '196px' },
              {},
              { height: '2px' },
            ]}
          />
        </ShapeContainer>
      </Grid>
      {!showLeftOnly && (
        <Grid item xs={6}>
          <ShapeContainer>
            <GaugeMeter
              id={'graph4-5'}
              color={['orange']}
              styles={[
                { transform: 'rotate(.35turn)' },
                { transform: 'rotate(0turn)' },
                { transform: 'rotate(.35turn)' },
                {},
                { transform: 'rotate(0turn)' },
              ]}
            />
            <GaugeMeter
              id={'graph4-6'}
              color={['yellow']}
              styles={[
                { transform: 'rotate(.21turn)' },
                { transform: 'rotate(0turn)' },
                { transform: 'rotate(.21turn)' },
                {},
                { transform: 'rotate(0turn)' },
              ]}
            />
          </ShapeContainer>
        </Grid>
      )}
    </Grid>
  );
}
