import React from 'react';

import { makeStyles } from '@material-ui/core/styles';

import clsx from 'clsx';
import globalStyles from '../../globalStyles';
import Config from '../../Config';

const duration = 25;
const colors = {
  blue: { backgroundColor: Config.colors.darkBlue },
  yellow: { backgroundColor: Config.colors.creme },
  orange: { backgroundColor: Config.colors.orange },
};
const useMainStyles = makeStyles((theme) => ({
  graphContainer: {
    position: 'relative',
    width: '90%',
    maxWidth: '400px',
    height: '200px',
    transform: 'scale(0.8)',
    [theme.breakpoints.between('xs', 'sm')]: {
      transform: 'scale(0.6) !important',
    },
    [theme.breakpoints.down('xs')]: {
      transform: 'scale(0.5) !important',
    },
  },
}));

// display a responsive container
export function ShapeContainer({ children }) {
  const classes = useMainStyles();
  const gS = globalStyles();
  return <div className={clsx(classes.graphContainer, gS.flexCenter)}>{children}</div>;
}

// return the percentage value for index to us as @keyframes key
function getPercentage(styles, index) {
  const values = {
    2: ['0%', '100%'],
    3: ['0%', '5O%', '100%'],
    4: ['0%', '33%', '66%', '100%'],
    5: ['0%', '25%', '50%', '75%', '100%'],
  };
  if (!styles || styles.length < 1) {
    return '0%';
  }
  return values[styles.length][index];
}

function useDotStyles(styles, noAnimation = false) {
  function init(theme, styles) {
    let myEffect = {};
    // styles[0] est deja utilisé
    if (!noAnimation) {
      for (let i = 1; i < styles.length; i++) {
        myEffect[getPercentage(styles, i)] = styles[i];
      }
    }

    return {
      dot: {
        position: 'absolute',
        height: '32px',
        width: '32px',
        borderRadius: '50%',
        margin: '0',
        '&:hover': {
          transition: 'transform 400ms',
          transform: 'scale(2) !important',
        },
        animation: `$dotEffect ${duration}s infinite ${theme.transitions.easing.easeInOut}`,
        transform: "translate3d(0,0,0)",
        
        ...styles[0],
      },
      '@keyframes dotEffect': myEffect,
      ...colors,
    };
  }

  let ms = makeStyles((theme) => init(theme, styles));
  return ms();
}

/**
 *
 * @param string id for debug
 * @param string color
 * @param array of styles used for animation
 * @param bool noAnimation , disalble animation, take only firt
 */
export function Dot({ id, color, styles, noAnimation = false }) {
  //styles = styles.reverse();

  const classes = useDotStyles(styles, noAnimation);

  return <div className={clsx(classes.dot, classes[color])} id={id}></div>;
}

function useEdgeStyles(style, height) {
  function init(theme, style, height) {
    let myEffect = {};

    /*
    // la hauteur final est `height`, on la coupe en 5
    const percentages = ['15%', '30%', '45%', '60px', '75%', '90%'];
    const heightPart = height / percentages.length;
    for (let i = 0; i < percentages.length; i++) {
      myEffect[percentages[i]] = { height: heightPart * i + 'px' };
    }
    */
    myEffect['10%'] = { height: height/20 + 'px' };
    myEffect['100%'] = { height: height + 'px' };


    return {
      edge: {
        position: 'absolute',
        width: '30px',
        borderRadius: '2%',
        margin: '0',
        backgroundColor: '#73879c',
        WebkitTransformOrigin: '0px 0px',
        KhtmlTransformOrigin: '0px 0px',
        MozTransformOrigin: '0px 0px',
        MsTransformOrigin: '0px 0px',
        transformOrigin: '0px 0px',
        animation: `$edgeEffect 6s infinite alternate ${theme.transitions.easing.easeInOut}`,
        ...style,
        height: '0px',
      },
      '@keyframes edgeEffect': myEffect,
      ...colors,
    };
  }
  let ms = makeStyles((theme) => init(theme, style, height));
  return ms();
}

export function Edge({ id, height, style, color, styles }) {
  const classes = useEdgeStyles(style, height);
  return <div className={clsx(classes.edge, classes[color])} id={id}></div>;
}

function useGaugeStyles(styles) {
  function init(theme, styles) {
    let myEffect = {};
    // styles[0] est deja utilsé
    for (let i = 1; i < styles.length; i++) {
      myEffect[getPercentage(styles, i)] = styles[i];
    }

    return {
      gauge: {
        display: 'inline-block',
        position: 'relative',
        overflow: 'hidden',
        marginTop: '15px',
        width: '20rem',
        height: '10rem',
        borderRadius: '2%',
        margin: '0',
      },

      meter: {
        width: '20rem',
        height: '10rem',
        top: '100%',
        transition: '0.5s',
        transformOrigin: 'center top',
        borderRadius: '0 0 12rem 12rem',

        ...styles[0],
      },
      '@keyframes edgeEffect': myEffect,
      ...colors,
    };
  }
  let ms = makeStyles((theme) => init(theme, styles));
  return ms();
}

//animation: `$edgeEffect ${duration} infinite  ${theme.transitions.easing.easeInOut}`,

export function GaugeMeter({ id, color, styles }) {
  const classes = useGaugeStyles(styles);

  return (
    <div className={classes.gauge} id={id}>
      <div className={clsx(classes.meter, classes[color])}></div>
    </div>
  );
}
