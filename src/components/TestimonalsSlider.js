import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import globalStyles from '../globalStyles';
import Config from '../Config';
import ImportedGraphics from '../ImportedGraphics';
import clsx from 'clsx';
import { Grid } from '@material-ui/core';
import Slider from '@farbenmeer/react-spring-slider';
import MaxWidthCentered from '../components/MaxWidthCentered';

const useStyles = makeStyles((theme) => ({
  backgrd: {
    margin: '15px 0px',
    width: '100%',
    minHeight: '800px',
    position: 'relative',
    color: 'white',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center',
    backgroundImage: `url(${ImportedGraphics.background_pattern})`,
    flexGrow: 1,
  },
  overlay: {
    content: '',
    position: 'absolute',
    top: '0',
    left: '0',
    width: '100%',
    minHeight: '100%',
    backgroundColor: `${Config.colors.orangeLight} !important`,
    opacity: '0.95',
    display: 'flex',
    flexGrow: 1,
  },
  margin25: {
    [theme.breakpoints.up('sm')]: {
      margin: 25,
      marginTop: 50,
    },
  },
  blockquote: {
    [theme.breakpoints.up('sm')]: {
      '&:before': {
        fontSize: '45px',
        color: 'grey',
        content: '"\u275E"',
      },
    },
    fontSize: 'calc(12px + 1vmin) !important',
    [theme.breakpoints.down('sm')]: {
      //fontSize: 'calc(8x + 1vmin) !important ',
      textAlign: 'justify',
    },
    boxSizing: 'border-box',
    color: 'rgb(42, 60, 70)',
    cursor: 'default',
    fontFamily: "'Montserrat', 'Source Sans Pro', 'sans-serif'",
    fontStyle: 'italic',
    lineHeight: '35.2px',
    marginBottom: '20px',
    textAlign: 'center',
    textRendering: 'optimizelegibility',
    overflowY: 'auto',
  },
  cite: {
    color: '#2a3c46',
    fontSize: '14px',
    lineHeight: '150%',
    fontWeight: '700',
  },
  citeAhref: {
    listStyle: 'none',
    marginLeft: '5px',
    color: 'inherit',
  },
}));

/**
 *
 * @param {*} param0
 */
export default function TestimonalsSlider({ id }) {
  const classes = useStyles();
  const gS = globalStyles();

  const _slides = [
    <blockquote>
      <p>
        Nous avons aussi été frappés par la variété d'entreprises coopératives issues tant du secteur industriel que de startups comme
        Mnemotix - née d'un projet de recherche de Sophia Antipolis -, ou 1DLab qui fait du streaming musical alternatif, incubée au Numa.
        Les formes coopératives sont intrinsèquement prédisposées à se déployer dans le numérique qui est par essence un terrain de création
        collective et d'innovation ouverte
      </p>
      <footer>
        <cite className={classes.cite}>
          Benoît Thieulin, président du Conseil National du Numérique,
          <a
            className={classes.citeAhref}
            href="http://www.cnnumerique.fr/wp-content/uploads/2015/12/Rapport-travail-version-finale-janv2016.pdf"
            target="_blank"
            rel="noreferrer"
          >
            discours du 6 janvier 2015
          </a>
        </cite>
      </footer>
    </blockquote>,

    <blockquote>
      <p>
        Le mouvement coopératif et les écosystèmes numériques gagneraient à se rapprocher davantage. Cette convergence constituerait une
        alternative positive aux craintes actuelles de captation de la valeur économique et sociale par de grands acteurs. Les modèles
        d'organisation en SCOP ou en SCIC sont particulièrement adaptés à des projets de plateformes numériques soutenables et équitables.
        Le mode de gouvernance proposée (sociétariat, intégration de parties prenantes publiques et privées) intègre ainsi l'innovation
        sociale au coeur de l'organisation de travail et n'en fait plus seulement une finalité.
      </p>
      <footer>
        <cite className={classes.cite}>
          Conseil National du Numérique - Dossier de presse -
          <a
            className={classes.citeAhref}
            target="_blank"
            rel="noreferrer"
            href="http://www.cnnumerique.fr/wp-content/uploads/2015/11/Dossier-de-presse-rapport-travail-emploi-CNNum-VF.pdf"
          >
            Rapport "Travail, Emploi, Numérique, les nouvelles trajectoires"
          </a>
          (janvier 2016)
        </cite>
      </footer>
    </blockquote>,
  ];
  const _title = 'Ils parlent de nous';

  return (
    <div className={clsx(classes.backgrd)} id={id || 'testimonials'}>
      <div className={clsx(classes.overlay)}>
        <Grid container direction="column" justifyContent="center" alignItems="center" className={clsx(classes.margin25)}>
          <Grid item xs={12} md={10} style={{ width: '100%' }}>
            <div className={clsx(gS.sectionTitle, gS.marginAuto, gS.darkBlueColor)}>{_title}</div>

            <Slider hasArrows hasBullets bulletStyle={{ backgroundColor: '#fff' }} auto={0}>
              {_slides.map((sld, index) => (
                <MaxWidthCentered key={index} className={clsx(gS.marginAuto, gS.darkBlueColor)}>
                  <div className={classes.blockquote}>{sld}</div>
                </MaxWidthCentered>
              ))}
            </Slider>
          </Grid>
        </Grid>
      </div>
    </div>
  );
}
