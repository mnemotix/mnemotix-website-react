import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Hidden } from '@material-ui/core';
import MaxWidthCentered from './MaxWidthCentered';
import Config from '../Config';
import FlexboxWrap from './FlexboxWrap';
import globalStyles from '../globalStyles';

const useStyles = makeStyles((theme) => ({
  footer: {
    minHeight: Config.footer.height,
    alignItems: 'center',
    backgroundColor: Config.colors.darkBlue,
    color: '#707e8c',
    display: 'flex',
    justifyContent: 'center',
  },
  text: {
    fontSize: '20px',
    lineHeight: '24px',
  },
  ahref: {
    listStyle: 'none',
    textDecoration: 'none',
    border: '1px solid rgba(255, 255, 255, 0)',
    borderRadius: '3px',
    zIndex: '1',
    fontSize: '20px',
    lineHeight: '24px',
    color: '#707e8c',
  },

  flex: {
    display: 'flex',
  },
  centerOrRow: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'space-between',
    width: "100%",
    [theme.breakpoints.between('sm', 'md')]: {
      flexDirection: 'column',
    },
  },
}));

export default function Footer() {
  const classes = useStyles();
  const gS = globalStyles();
  return (
    <footer className={classes.footer}>
       <MaxWidthCentered>
        <div className={classes.centerOrRow}>
          <div className={gS.m10}>
            <a href="/" className={classes.ahref}>
              © {new Date().getFullYear()} mnemotix, tous droits réservés.
            </a>
          </div>
          <Hidden smDown>
            <FlexboxWrap>
              <>
                <div className={gS.m10}>
                  <a className={classes.ahref} href="/">
                    Accueil
                  </a>
                </div>
                <div className={gS.m10}>
                  <a className={classes.ahref} href="/#services">
                    Services
                  </a>
                </div>
                <div className={gS.m10}>
                  <a className={classes.ahref} href="/#clients">
                    Clients
                  </a>
                </div>
                <div className={gS.m10}>
                  <a className={classes.ahref} href="/#apropos">
                    A propos
                  </a>
                </div>
                <div className={gS.m10}>
                  <a className={classes.ahref} href="/#contact">
                    Contact
                  </a>
                </div>
              </>
            </FlexboxWrap>
          </Hidden>
        </div>
        </MaxWidthCentered>
    </footer>
  );
}
