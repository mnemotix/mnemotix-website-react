import React from 'react';
import { Waypoint } from 'react-waypoint';

/**
 * composant qui permet de changer l'url quand on scroll dessus
 * @param {*} param0
 */
export default function WaypointSection({ hash, setCurrentAnchor, children, ...props }) {
  return (
    <div id={hash}>
      <Waypoint
        onEnter={() => {
          setCurrentAnchor(hash);
        }}
        bottomOffset={'40%'}
        key={'#hash'}
      >
        <div>{children}</div>
      </Waypoint>
    </div>
  );
}
