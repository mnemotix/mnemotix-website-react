import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import MaxWidthCentered from './MaxWidthCentered';
import globalStyles from '../globalStyles';
import clsx from 'clsx';
import Config from '../Config';

const useStyles = makeStyles((theme) => ({
  backgrd: {
    backgroundColor: Config.colors.white,
    color: Config.colors.darkBlue,
  },
  fsize: {
    fontSize: 'calc(10px + 1vmin) !important',
  },
}));

/**
 * display the content of a section with MaxWidthCentered
 * @param {bool} nobackground : remove background color
 */
export default function SectionContent({ nobackground = false, children, ...props }) {
  const classes = useStyles();
  const gS = globalStyles();

  return (
    <div className={clsx(!nobackground && classes.backgrd, classes.fsize, gS.paddingTB50)}>
      <MaxWidthCentered {...props}>{children}</MaxWidthCentered>
    </div>
  );
}
