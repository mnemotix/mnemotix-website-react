import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import globalStyles from '../globalStyles';
import clsx from 'clsx';
import Config from '../Config';

import ImportedGraphics from '../ImportedGraphics';
const bgFiles = {
  orange: { img: `url(${ImportedGraphics.background_orange})`, color: Config.colors.orange },
  blue: {
    img: `url(${ImportedGraphics.background_blue})`,
    color: Config.colors.darkBlue,
  },
};

const useStyles = makeStyles((theme) => ({
  backgrd: {
    margin: '50px 0px',
    width: '100%',
    height: '250px',
    position: 'relative',
    backgroundSize: 'cover',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: '25%',
    color: 'white',
  },
}));

/**
 *
 * @param {*} param0
 */
export default function SectionTitle({ children, color, title, subTitle, ...props }) {
  const classes = useStyles();
  const gS = globalStyles();

  const backgrdImg = {
    // display a color then a background over it for slow connection
    backgroundColor: bgFiles[color].color,
    backgroundImage: bgFiles[color].img,
  };

  return (
    <div className={clsx(classes.backgrd, gS.flexCenter, gS.flexDirectionCol)} style={backgrdImg} {...props}>
      <div className={gS.sectionTitle}>{title}</div>
      <div className={gS.sectionSubTitle}>{subTitle}</div>
      {children}
    </div>
  );
}
