import React, { useState } from 'react';
import { createMuiTheme, ThemeProvider, makeStyles } from '@material-ui/core/styles';
import { AppBar, Toolbar, IconButton, Typography, MenuItem, Menu } from '@material-ui/core';

import useScrollTrigger from '@material-ui/core/useScrollTrigger';
import MaxWidthCentered from './MaxWidthCentered';
import FlexboxWrap from './FlexboxWrap';
import MenuIcon from '@material-ui/icons/Menu';
import TwitterIcon from '@material-ui/icons/Twitter';
import clsx from 'clsx';
import logo from '../assets/images/logo.svg';
import Config from '../Config';
import globalStyles from '../globalStyles';

const theme = createMuiTheme({
  overrides: {
    MuiPopover: {
      paper: {
        width: '100% !important',
        maxWidth: '100% !important',
        height: '80%',
        maxHeight: 'unset',
        left: '0% !important',
      },
    },
  },
});

const useStyles = makeStyles((theme) => ({
  appbar: {
    height: Config.navbar.height,
    alignItems: 'center',
    backgroundColor: 'white',
    color: Config.colors.darkBlue,
    display: 'flex',
    justifyContent: 'center',
  },
  smallAppbar: {
    height: `${Config.navbar.height / 1.5}px`,
    transition: 'all 400ms ease-out',
  },
  logoContainer: {
    display: 'flex',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  logo: {
    height: `${Config.navbar.height / 1.2}px`,
    maxHeight: `${Config.navbar.logoMaxHeight}px`,
    maxWidth: '65vw',
    margin: 'auto',
    paddingLeft: '20px',
  },
  smallLogo: {
    width: 'min(50vw, 250px)',
  },

  menuIcon: {
    width: '10vw',
    height: '10vw',
    padding: '2vw 1vh',
    maxHeight: `${Config.navbar.height / 3}px`,
    maxWidth: `${Config.navbar.height / 3}px`,
  },

  menuButton: {
    marginRight: theme.spacing(2),
  },

  ahref: {
    /*display: 'none',
        [theme.breakpoints.up('sm')]: {
            display: 'block',
        },*/
    listStyle: 'none',
    fontSize: '15px',
    lineHeight: '20px',
    fontWeight: '700',
    textTransform: 'uppercase',
    textDecoration: 'none',
    border: '1px solid rgba(255, 255, 255, 0)',
    borderRadius: '3px',
    zIndex: '1',
    color: Config.colors.darkBlue,
  },

  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('lg')]: {
      display: 'flex',
      //flex: 1,
      marginLeft: '15px',
    },
  },
  sectionMobile: {
    display: 'flex',
    flex: 1,
    alignItems: 'center',
    justifyContent: 'flex-end',
    [theme.breakpoints.up('lg')]: {
      display: 'none',
    },
  },
}));

/**
 * header avec la barre de navigation
 */
export default function NavHeader() {
  const classes = useStyles();
  const gS = globalStyles();
  const [mobileMoreAnchorEl, setMobileMoreAnchorEl] = useState(null);
  // pour ouvrir/fermer le menu
  const isMobileMenuOpen = Boolean(mobileMoreAnchorEl);
  const handleMobileMenuClose = () => {
    setMobileMoreAnchorEl(null);
  };
  const handleMobileMenuOpen = (event) => {
    setMobileMoreAnchorEl(event.currentTarget);
  };

  // au scroll dans la page on reduit la taille du logo principal et la hauteur du header
  const showSmallHeader = useScrollTrigger();

  const entries = [
    <a className={classes.ahref} href="/#methodologie">
      Méthodologie
    </a>,
    <a className={classes.ahref} href="/#services">
      Services
    </a>,
    <a className={classes.ahref} href="/#apropos">
      Mnemotix
    </a>,
    <a className={classes.ahref} href="/#clients">
      Clients
    </a>,
    <a className={classes.ahref} href="/#realisations">
      Réalisations
    </a>,
    <a className={classes.ahref} href="/#recrutement">
      Recrutement
    </a>,
    <a className={classes.ahref} href="/#contact">
      Contact
    </a>,
    <a className={classes.ahref} href="https://twitter.com/mnemotix" target="blank" ahref="See us on Twitter">
      <TwitterIcon />
    </a>,
  ];

  // affiche le menu en mode mobile uniquement via popover
  const mobileMenuId = 'primary-search-account-menu-mobile';
  const renderMobileMenu = (
    <ThemeProvider theme={theme}>
      <Menu
        anchorReference="anchorPosition"
        anchorPosition={{ top: Config.navbar.height, left: 0 }}
        /*        anchorEl={mobileMoreAnchorEl}
                        anchorOrigin={{vertical: 'bottom', horizontal: 'right'}}    */
        id={mobileMenuId}
        keepMounted
        transformOrigin={{ vertical: 'top', horizontal: 'right' }}
        open={isMobileMenuOpen}
        onClose={handleMobileMenuClose}
      >
        {entries.map((item, index) => (
          <MenuItem key={index} onClick={handleMobileMenuClose}>
            <div className={gS.m5}>
              <Typography variant="h6" noWrap>
                {item}
              </Typography>
            </div>
          </MenuItem>
        ))}
      </Menu>
    </ThemeProvider>
  );

  return (
    <div>
      <AppBar position="fixed" className={clsx(classes.appbar, showSmallHeader && classes.smallAppbar)}>
        <MaxWidthCentered>
          <Toolbar disableGutters={true} className={gS.flexSpaceBetween}>
            <a className={classes.logoContainer} href="/">
              <img alt="Mnemotix" src={logo} className={clsx(classes.logo, showSmallHeader && classes.smallLogo)} />
            </a>
            <div className={classes.sectionDesktop}>
              <FlexboxWrap>
                {entries.map((item, index) => (
                  <div key={index} className={clsx(gS.m10, gS.flexCenter)}>
                    {item}
                  </div>
                ))}
              </FlexboxWrap>
            </div>
            <div className={classes.sectionMobile}>
              <IconButton
                className={classes.largeIcon}
                aria-label="show more"
                aria-controls={mobileMenuId}
                aria-haspopup="true"
                onClick={handleMobileMenuOpen}
                color="inherit"
              >
                <MenuIcon className={classes.menuIcon} />
              </IconButton>
            </div>
          </Toolbar>
        </MaxWidthCentered>
      </AppBar>
      {renderMobileMenu}
    </div>
  );
}
