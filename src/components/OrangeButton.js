import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import Config from '../Config';

const useStyles = makeStyles((theme) => ({
  margin: {
    color: Config.colors.darkBlue,
    backgroundColor: Config.colors.orange,
    '&:hover': {
      backgroundColor: Config.colors.orangeDarker,
    },
    marginTop: '2vw',
    marginBottom: '2vw',
    padding: 'min(20px,5vw)',
    fontWeight: '700',
    fontSize: '16',
    letterSpacing: '0.02em',
    textTransform: 'uppercase',
  },
}));

export default function OrangeButton({ children, ...props }) {
  const classes = useStyles();
  return (
    <Button {...props} variant="contained" className={classes.margin}>
      {children}
    </Button>
  );
}
