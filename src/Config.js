const Config = {
  colors: {
    darkBlue: '#2a3c46',
    blue :'#10263D',
    orange: "#f29e60ff", //'#F4A15D',
    orangeDarker: '#ED8F4B',
    orangeLight: '#f4ebe4',
    greenMnb: '#50833b',
    creme: '#f8f2ec',
    white: 'white',
    weeverGrey : "#a8a8a8"
  },
  navbar: {
    height: 115,
    logoMaxHeight: 85,
  },
  footer: {
    height: 100,
  }
};
export default Config;
