import Config from './Config';
import { makeStyles } from '@material-ui/core/styles';

const globalStyles = makeStyles((theme) => ({
  tester: {
    border: '1px solid green',
    padding: '1px',
  },
  tester2: {
    border: '2px dotted red',
    padding: '1px',
  },
  flexCenter: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    //flex: 1,
  },
  flexSpaceBetween: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'space-between',
    flex: 1,
    width: '100%',
  },
  flexStart: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-start',
    width: '100%',
    flex: 1,
  },
  flexEnd: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    flex: 1,
  },
  flexDirectionCol: {
    flexDirection: 'column',
  },

  width100: {
    width: '100%',
  },
  maxWidth: {
    width: '100%',
    maxWidth: '60vw',
    // padding: '10px',
    [theme.breakpoints.between('sm', 'lg')]: {
      maxWidth: '70vw',
    },
    [theme.breakpoints.between('xs', 'sm')]: {
      maxWidth: '80vw',
    },
    [theme.breakpoints.down('xs')]: {
      maxWidth: '85vw',
    },
  },
  grow: {
    flexGrow: 1,
  },
  p10: {
    padding: '10px !important',
  },
  p20: {
    padding: '20px !important',
  },
  paddingTB25: {
    paddingTop: '25px !important',
    paddingBottom: '25px !important',
  },
  paddingT35: {
    paddingTop: '35px !important',
  },
  paddingB50: {
    paddingBottom: '50px !important',
  },

  paddingT50: {
    paddingTop: '50px !important',
  },
  paddingTB50: {
    paddingTop: '50px !important',
    paddingBottom: '50px !important',
  },
  m10: {
    margin: '10px !important',
  },
  m5: {
    margin: '5px !important',
  },
  marginTB50: {
    marginTop: '50px !important',
    marginBottom: '50px !important',
  },
  marginAuto: {
    margin: 'auto',
  },

  textAlignLeft: {
    textAlign: 'left !important',
  },
  textAlignRight: {
    textAlign: 'right !important',
  },
  textAlignCenter: {
    textAlign: 'center !important',
  },
  textJustify: {
    textAlign: 'justify  !important',
  },
  lightColor: {
    color: '#fff !important',
  },
  darkBlueColor: {
    color: Config.colors.darkBlue,
  },
  aUnderline: {
    textDecoration: 'underline',
    color: 'inherit',
  },
  aNoDecoration: {
    textDecoration: 'none',
    color: 'inherit',
  },

  subText: {
    letterSpacing: '0.015em',
    textTransform: 'none',
    textAlign: 'left',
    marginTop: '20px',
    lineHeight: '150%',
    fontSize: 'calc(10px + 2vmin)!important',
    fontWeight: '400',
  },
  halfHeight: {
    height: '50%',
  },
  height100: {
    height: '100%',
  },
  sectionTitle: {
    fontSize: 'calc(10px + 3vmin) !important',
    lineHeight: '130%',
    marginTop: '15px',
    marginBottom: '25px',
    textAlign: 'center',
    fontWeight: '700',
    letterSpacing: '0.025em',
    [theme.breakpoints.down('sm')]: {
      fontWeight: '500',
      letterSpacing: '0.0125em',
    },
  },
  sectionSubTitle: {
    textAlign: 'center',
    fontSize: '24px',
    [theme.breakpoints.down('sm')]: {
      fontSize: '18px',
    },
  },
  orangeBackgroundColor: {
    backgroundColor: Config.colors.orange,
    padding: '15px 20px',
  },

  // sectionContent
  sc_titleUpper: {
    fontWeight: 700,
    fontSize: 'calc(10px + 4vmin) !important',
    padding: 25,
    letterSpacing: '1.1px',
    lineHeight: '58px',
    textAlign: 'center',
    textRendering: 'optimizelegibility',
    textTransform: 'uppercase',
  },

  // image & icon
  img50: {
    width: '50px',
    height: '50px',
    borderRadius: '10%',
  },
  img100: {
    width: 'min(100px,15vw) !important',
    height: 'min(100px,15vw) !important',
    borderRadius: '10%',
  },
  img200: {
    width: '200px',
    height: '200px',
    borderRadius: '20%',
  },
  responsiveImage: {
    width: '95%',
    height: 'auto',
  },
  responsiveImageW80: {
    width: '80%',
    height: 'auto',
  },
  responsiveImageH80: {
    height: '70%',
    width: 'auto',
  },
  responsiveImageM500: {
    width: '80%',
    maxWidth: '500px',
    height: 'auto',
  },

  // pages only
  pages_mainImg: {
    display: 'block',
    margin: 'auto',
    maxHeight: '80%',
    width: '75vw',
    maxWidth: '1150px',
  },
  pages_darkblueBgColor: {
    backgroundColor: '#2a3c46',
    color: 'white',
    padding: '15px',
    paddingLeft: '20px',
    paddingRight: '20px',
    display: 'inline-block',
    textAlign: 'left !important',
  },
  pages_marginH35v25: {
    margin: '35px',
    marginLeft: '25px',
    marginRight: '25px',
    [theme.breakpoints.between('sm', 'md')]: {
      margin: '10px',
    },
    [theme.breakpoints.down('sm')]: {
      margin: '5px',
    },
  },
  pages_videoResponsive: {
    overflow: 'hidden',
    paddingBottom: '56.25%',
    position: 'relative',
    height: '0',
  },
  pages_img50: {
    marginLeft: 'min(25px, 1vw)',
    marginRight: '25px',
    width: 'min(45px,5vh)',
    height: 'min(45px,5vh)',
    borderRadius: '10%',
  },
  pages_mCenter: {
    marginTop: 'auto',
    marginBottom: 'auto',
  },

  ahref: {
    listStyle: 'none',
    // textDecoration: 'none',
    color: Config.colors.darkBlue,
  },
}));

export default globalStyles;
