import background_pattern from '../public/images/background/pattern.jpg';
import background_orange from '../public/images/background/orange.jpg';
import background_blue from '../public/images/background/blue.jpg';
import background_slider_graph from '../public/images/background/slider-graph.jpg';
import ddf_slider_back from '../public/images/projects/ddf/slider-back.jpg';
import ddf_tabtext from '../public/images/projects/ddf/tab-text.png';
import ddf_logo from '../public/images/projects/ddf/logo.png';
import ddf_smartphone from '../public/images/projects/ddf/smartphoneonly.png';
import weever_column_logo from '../public/images/projects/weever/weever_column_logo.svg';
import weever_icon_a from '../public/images/projects/weever/weever_icon_a.svg';
import weever_icon_b from '../public/images/projects/weever/weever_icon_b.svg';
import weever_icon_c from '../public/images/projects/weever/weever_icon_c.svg';
import weever_icon_d from '../public/images/projects/weever/weever_icon_d.svg';
import weever_slider_back from '../public/images/projects/weever/slider-backd.jpg';

import mnb_slider_back from '../public/images/projects/mnb/slider-back.jpg';
import mnb_project_back from '../public/images/projects/mnb/project-back.jpg';
import mnb_tab_smart from '../public/images/projects/mnb/tab-smart.png';
import mnb_ordi from '../public/images/projects/mnb/ordi.png';

import mnb_site_demo_1 from '../public/images/projects/mnb/site-demo-1.jpg';
import mnb_site_demo_2 from '../public/images/projects/mnb/site-demo-2.jpg';
import mnb_logo from '../public/images/projects/mnb/logo.png';

import art_carre from '../public/images/art/carre.png';
import art_developpment from '../public/images/art/developpment.png';
import art_formation from '../public/images/art/formation.png';
import art_herbergement from '../public/images/art/herbergement.png';
import art_triangle from '../public/images/art/triangle.png';
import art_carreorange from '../public/images/art/carreorange.png';
import art_croix from '../public/images/art/croix.png';
import art_consulting from '../public/images/art/consulting.png';
import art_modelisation from '../public/images/art/modelisation.png';
import art_triangleorange from '../public/images/art/triangleorange.png';

import clients_ademe from '../public/images/clients/ademe.png';
import clients_tgc from '../public/images/clients/tgc.png';
import clients_fegl from '../public/images/clients/fegl.png';
import clients_cnrs from '../public/images/clients/cnrs.png';
import clients_inria from '../public/images/clients/inria.png';
import clients_sictiam from '../public/images/clients/sictiam.png';
import clients_infotel from '../public/images/clients/infotel.png';
import clients_psa from '../public/images/clients/psa.png';
import clients_mindmatcher from '../public/images/clients/mindmatcher.png';
import clients_1dlab from '../public/images/clients/1dlab.png';
import clients_isthia from '../public/images/clients/isthia.png';
import clients_va from '../public/images/clients/va.png';
import clients_uca from '../public/images/clients/uca.png';
import clients_ujjt from '../public/images/clients/ujjt.png';
import clients_ministereculture from '../public/images/clients/ministereculture.png';
import clients_educlever from '../public/images/clients/educlever.png';
import clients_corekap from '../public/images/clients/corekap.png';
import clients_clairsienne from '../public/images/clients/clairsienne.png';
import clients_utc from '../public/images/clients/utc.png';
import clients_2if from '../public/images/clients/2if.png';

import illustrations_01 from '../public/images/illustrations/01.png';
import illustrations_03 from '../public/images/illustrations/03.png';
import illustrations_02 from '../public/images/illustrations/02.png';

import partners_devops from '../public/images/partners/devops.jpg';
import partners_elzeard from '../public/images/partners/elzeard.jpg';
import partners_ontotext from '../public/images/partners/ontotext.jpg';
import partners_winmics from '../public/images/partners/winmics.jpg';

import team_aibrahim from '../public/images/team/aibrahim.jpg';
import team_flimpens from '../public/images/team/flimpens.jpg';
import team_mleitzelman from '../public/images/team/mleitzelman.png';
import team_mrogelja from '../public/images/team/mrogelja.png';
import team_ndelaforge from '../public/images/team/ndelaforge.png';
import team_prlherisson from '../public/images/team/prlherisson.png';

const ImportedGraphics = {
  background_pattern,
  background_orange,
  background_blue,
  background_slider_graph,
  weever: {weever_column_logo, weever_slider_back, weever_icon_a, weever_icon_b, weever_icon_c, weever_icon_d},
  art_carreorange,
  art_carre,
  art_developpment,
  art_formation,
  art_herbergement,
  art_triangle,
  art_carreorange,
  art_croix,
  art_consulting,
  art_modelisation,
  art_triangleorange,
  mnb_ordi,
  mnb_project_back,
  mnb_tab_smart,
  mnb_site_demo_1,
  mnb_site_demo_2,
  mnb_logo,
  mnb_slider_back,
  ddf_logo,
  ddf_slider_back,
  ddf_tabtext,
  ddf_smartphone,
  clients_ademe,
  clients_tgc,
  clients_fegl,
  clients_cnrs,
  clients_inria,
  clients_sictiam,
  clients_infotel,
  clients_psa,
  clients_mindmatcher,
  clients_1dlab,
  clients_isthia,
  clients_va,
  clients_uca,
  clients_ujjt,
  clients_ministereculture,
  clients_educlever,
  clients_corekap,
  clients_clairsienne,
  clients_utc,
  clients_2if,
  illustrations_01,
  illustrations_03,
  illustrations_02,
  partners_devops,
  partners_elzeard,
  partners_ontotext,
  partners_winmics,
  team_aibrahim,
  team_flimpens,
  team_mleitzelman,
  team_mrogelja,
  team_ndelaforge,
  team_prlherisson,
};
export default ImportedGraphics;

/*
import   from "../public/images/";
import   from "../public/images/";

import ImportedGraphics from '../ImportedGraphics';

src={`url(${ImportedGraphics.ddf_tabtext})`}

*/
