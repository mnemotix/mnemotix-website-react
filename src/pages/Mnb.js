import React, { useEffect } from 'react';
import { Grid } from '@material-ui/core';
import clsx from 'clsx';
import Banner from '../components/Banner';
import SectionContent from '../components/SectionContent';
import CircleText from '../components/CircleText';
import globalStyles from '../globalStyles';
import ImportedGraphics from '../ImportedGraphics';

export default function Mnb() {
  const gS = globalStyles();
  useEffect(() => {
    document.title = 'BioMétéo Dordogne.';
  }, []);
  return (
    <div className={clsx(gS.flexStart, gS.flexDirectionCol, gS.width100)}>
      <Banner
        style={{
          backgroundImage: `url(${ImportedGraphics.mnb_project_back})`,
        }}
      >
        <div className={gS.flexEnd}>
          <img src={ImportedGraphics.mnb_tab_smart} className={gS.pages_mainImg} alt="" />
        </div>
      </Banner>

      <SectionContent>
        <div className={gS.width100}>
          <div className={gS.paddingTB25}>
            <h2 className={gS.pages_darkblueBgColor}>La BioMétéo et le Hub de données biodiversité</h2>
            <div className={gS.pages_marginH35v25}>               
              L'application Biométéo (disponible sur{' '}
              <a href="https://biometeo.dordogne.fr/" target="_blank" className={gS.ahref}>
                https://biometeo.dordogne.fr/
              </a>
              ) est une application web responsive qui propose une « météo de la biodiversité ». La Biométéo a pour objectif de sensibiliser
              le grand public à la biodiversité et l'accompagner vers une meilleure connaissance et prise en compte de l'écologie au
              quotidien. L'application s'inscrit dans une pratique quotidienne (consultation de la météo) avec la diffusion d'informations
              actualisées sur l'état des observations et l'environnement (air, eau) et la biodiversersité locales à travers une sélection
              d'espèces observées localement.
              <br />
              <br />
              L'application repose sur un hub de données de biodiversité construit sur la base de notre middleware sémantique Synaptix qui
              agrège des données de diverses provenances et formats dans un graphe cohérent et suivant les standards du Web de Données Liées
              (
              <a href="https://fr.wikipedia.org/wiki/Linked_open_data" target="_blank" className={gS.ahref}>
                Linked Open Data
              </a>
              ).
            </div>
            <div className={clsx(gS.pages_marginH35v25)}>
              <img src={ImportedGraphics.mnb_site_demo_1} className={gS.width100} alt="" />
              <img src={ImportedGraphics.mnb_site_demo_2} className={gS.width100} alt="" />
            </div>
          </div>

          <div className={gS.paddingTB25}>
            <h2 className={gS.pages_darkblueBgColor}>Les clients </h2>
            <div className={gS.pages_marginH35v25}>
              Le projet BioMétéo fait partie de la <a href="https://www.dordogne.fr/relever-les-defis-du-21e-siecle/lexcellence-environnementale/biodiversite/maison-numerique-de-la-biodiversite-une-boite-a-outils-numeriques-au-service-de-lexcellence-environnementale" target="_blank" className={gS.ahref}>
              Maison Numérique de la Biodiversité
              </a> qui est portée par le Département de la Dordogne en
              partenariat avec d'autres acteurs territoriaux ou fournisseurs de données: BRGM, Museum National d'Histoire Naturel de Paris
              ou le Global Biodiversity Information Group (GBIF).
            </div>
          </div>

          <div className={gS.paddingTB25}>
            <h2 className={gS.pages_darkblueBgColor}>Les objectifs</h2>
            <div className={gS.pages_marginH35v25}>
              <Grid container direction="row" justifyContent="center" alignItems="center">
                <Grid item xs={12} md={6} className={clsx(gS.flexCenter, gS.paddingB50)}>
                  <CircleText>
                    Sensibiliser à travers le lien entre biodiversité et caractéristiques évolutives de notre environnement.
                  </CircleText>
                </Grid>
                <Grid item xs={12} md={6} className={clsx(gS.flexCenter, gS.paddingB50)}>
                  <CircleText secondColor>
                    Proposer une vitrine des meilleurs pratiques en agrégation et partage des données du domaine public.
                  </CircleText>
                </Grid>
              </Grid>
            </div>
          </div>

          <div className={gS.paddingB50}>
            <h2 className={gS.pages_darkblueBgColor}>Les détails de la prestation</h2>
            <div className={gS.pages_marginH35v25}>
              <Grid container direction="row" justifyContent="flex-start" alignItems="flex-start" spacing={1}>
                <Grid item xs={2} sm={1} className={gS.pages_mCenter}>
                  <img src={ImportedGraphics.art_carreorange} className={gS.pages_img50} alt="" />
                </Grid>
                <Grid item xs={10} sm={11} className={gS.pages_mCenter}>
                  Audit des données de biodiversité ouvertes et exploitables, création d'une ontologie permettant de relier données
                  environnementales et de biodiversité, conception et réalisation du Hub de Données offrant un accès SPARQL et GraphQL aux
                  données.
                </Grid>
              </Grid>
            </div>
            <div className={gS.pages_marginH35v25}>
              <Grid container direction="row" justifyContent="flex-start" alignItems="flex-start" spacing={1}>
                <Grid item xs={2} sm={1} className={gS.pages_mCenter}>
                  <img src={ImportedGraphics.art_triangleorange} className={gS.pages_img50} alt="" />
                </Grid>
                <Grid item xs={10} sm={11} className={gS.pages_mCenter}>
                  Co-conception avec les futurs usagers de l'application BioMétéo à travers des ateliers participatifs, pilotage du design
                  UX, intégration de la maquette graphique et réalisation de l'application web responsive.
                </Grid>
              </Grid>
            </div>
          </div>
        </div>
      </SectionContent>
    </div>
  );
}
