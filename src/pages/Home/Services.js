import React from 'react';
import globalStyles from '../../globalStyles';
import Grid from '@material-ui/core/Grid';
import SectionContent from '../../components/SectionContent';
import useGridClasses from '../../utilities/useGridClasses';
import clsx from 'clsx';
import ImportedGraphics from '../../ImportedGraphics';

export default function Services() {
  const gS = globalStyles();
  const { grid, className, parentClassName } = useGridClasses();

  return (
    <SectionContent>
      <div className={clsx(parentClassName, gS.textJustify)}>
        <Grid container {...grid}>
          <Grid item xs={12} md={6} className={className}>
            <img alt="" src={ImportedGraphics.art_developpment} className={gS.img100} />
            <h2>Développement</h2>
            <p>
              Nous construisons des solutions innovantes, personnalisées et respectueuses des standards du web. Nous intervenons à tous les
              niveaux, du stockage de la donnée à sa visualisation et traitons de grand volumes.
            </p>
          </Grid>
          <Grid item xs={12} md={6} className={className}>
            <img alt="" src={ImportedGraphics.art_herbergement} className={gS.img100} />
            <h2>Hébergement</h2>
            <p>
              Notre infrastructure de production, à la fois souple et performante, est capable de supporter le déploiement de tous nos
              développements en garantissant des niveaux de performance et de sécurité élevés.
            </p>
          </Grid>

          <Grid item xs={12} md={6} className={className}>
            <img alt="" src={ImportedGraphics.art_consulting} className={gS.img100} />
            <h2>Consulting</h2>
            <p>
              Nous vous conseillons et auditons vos systèmes d'information. Nous pouvons aussi vous accompagner dans la conception de
              nouvelles architectures logicielles et dans la mise à niveau des architectures existantes (Assistance à maitrise d'ouvrage).
            </p>
          </Grid>

          <Grid item xs={12} md={6} className={className}>
            <img alt="" src={ImportedGraphics.art_modelisation} className={gS.img100} />

            <h2>Modélisation</h2>
            <p>
              Première étape sur le chemin de l'Open Data et du Web des données, nous vous aidons à structurer les concepts centraux de
              l'entreprise. Nous vous accompagnons grâce à des outils éprouvés.
            </p>
          </Grid>
        </Grid>
        <Grid container {...grid}>
          <Grid item xs={12} md={6} className={className}>
            <img alt="" src={ImportedGraphics.art_formation} className={gS.img100} />
            <h2>Formation</h2>
            <p>
              Forte de son expertise à la fois scientifique et technologique en Ingénierie des Connaissances, Mnemotix développe depuis ses
              débuts une offre de formations couvrant un large panel de niveaux, de l'introduction à la modélisation des connaissances,
              jusqu'à l'usage avancé des technologies Big Data (GraphQL, ElasticSearch, etc.) et du Web Sémantique (RDF, SPARQL, etc.).
              Consultez{' '}
              <a
                className={gS.aUnderline}
                href="https://gitlab.com/mnemotix/formations/-/raw/master/catalogue/Book-Formations-Mn%C3%A9motix-2020.pdf?inline=false"
                target="_blank"
                rel="noreferrer"
              >
                notre catalogue
              </a>
              , et n'hésitez pas à{' '}
              <a className={gS.aUnderline} href="#contact">
                nous contacter
              </a>{' '}
              pour une formation personnalisée.
            </p>
          </Grid>
        </Grid>
      </div>
    </SectionContent>
  );
}
