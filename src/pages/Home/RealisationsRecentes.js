import React from 'react';
import globalStyles from '../../globalStyles';
import Grid from '@material-ui/core/Grid';
import SectionContent from '../../components/SectionContent';
import useGridClasses from '../../utilities/useGridClasses';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import ImportedGraphics from '../../ImportedGraphics';
import Config from '../../Config';

const useStyles = makeStyles((theme) => ({
  img: {
    width: '250px',
    maxWidth: '70vw',
    height: 'auto',
    borderRadius: '10%',
  },
  greenText: { color: Config.colors.greenMnb },
}));

export default function RealisationsRecentes() {
  const classes = useStyles();
  const gS = globalStyles();
  const { grid, className, parentClassName } = useGridClasses();
 
  return (
    <SectionContent>
      <div className={clsx(parentClassName, gS.textJustify)}>
        <Grid container {...grid}>
          <Grid item xs={12} md={6} className={clsx(className)}>
            <a href={"/ddf"} target={'_self'} className={gS.aNoDecoration}>
              <img alt="" src={ImportedGraphics.ddf_logo} className={classes.img} />
            </a>
            <a href={"/ddf"} target={'_self'} className={gS.aNoDecoration}>
              <h2>Le dictionnaire des francophones</h2>
            </a>
            <div>
            Le Dictionnaire des francophones est un dictionnaire collaboratif numérique et open source qui a pour objectif de rendre compte de la richesse du français parlé au sein de l'espace francophone. C'est un projet institutionnel novateur qui présente d'une part une partie de consultation au sein de laquelle sont compilées plusieurs ressources lexicographiques (avec plus de 440 000 mots et 610 000 définitions), et d'autre part une partie collaborative pour développer les mots et faire vivre la langue française.
            </div>
          </Grid>

          <Grid item xs={12} md={6} className={clsx(className)}>
            <a href="/mnb" className={gS.aNoDecoration}>
              <img alt="" src={ImportedGraphics.mnb_logo} className={classes.img} />
            </a>
            <a href="/mnb" className={gS.aNoDecoration}>
              <h2>
                La <span className={classes.greenText}>biodiversité</span> près de chez vous
              </h2>
            </a>
            <div>
              Le projet BioMétéo est principalement porté par le Département de Dordogne. Cette « météo de la biodiversité » a pour objectif
              de sensibiliser le grand public à la biodiversité et l'accompagner vers une meilleure connaissance et prise en compte de
              l'écologie au quotidien.
            </div>
          </Grid>
        </Grid>
      </div>
    </SectionContent>
  );
}
