import React from 'react';
import globalStyles from '../../globalStyles';
import Grid from '@material-ui/core/Grid';
import SectionContent from '../../components/SectionContent';
import useGridClasses from '../../utilities/useGridClasses';
import clsx from 'clsx';

export default function Apropos() {
  const gS = globalStyles();
  const { grid, className, parentClassName } = useGridClasses();

  return (
    <SectionContent>
      <div className={clsx(parentClassName, gS.textJustify)}>
        <Grid container {...grid}>
          <Grid item xs={12} md={6} className={className}>
            <h2>Une smart-up coopérative</h2>
            <p>
              Nous sommes une coopérative (une société d'intérêt collectif - SCIC), un modèle d'innovation sociale, notre gouvernance est
              transparente et regroupe salariés-associés, des clients bénéficaires, des experts et des partenaires divers.
            </p>
          </Grid>

          <Grid item xs={12} md={6} className={className}>
            <h2>Une équipe issue de la recherche</h2>
            <p>
              Notre équipe interdisciplinaire est majoritairement issue de la recherche française (UTC, INRIA, CNRS, Telecom ParisTech) et
              continue à y entretenir des relations étroites. Ce lien privilégié nous permet notamment de participer à des projets de
              recherche nationaux ou européens, et de faire bénéficier à nos clients d'une technologie toujours à la pointe dans le domaine
              du web et des données.
            </p>
          </Grid>
        </Grid>
        <Grid container {...grid}>
          <Grid item xs={12} md={6} className={className}>
            <h2>Des technologies high tech et open source</h2>
            <p>
              Nous proposons des solutions basées sur des technologies matures ques vos équipes techniques pourront prendre en main avec
              notre aide.
              <br />
              La dimension open source rend la solution durable et vous permettra de devenir autonomes.
            </p>
          </Grid>

          <Grid item xs={12} md={6} className={className}>
            <h2>La création de communs logiciels</h2>
            <p>Vous pouvez mutualiser vos solutions pour obtenir des outils performants et durables, tout en partageant les coûts.</p>
          </Grid>
        </Grid>
      </div>
    </SectionContent>
  );
}
