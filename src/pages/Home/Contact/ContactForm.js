import React, { useState } from 'react';
import { Field, Form, Formik } from 'formik';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import OrangeButton from '../../../components/OrangeButton';
import * as axios from '../../../utilities/axios';
import * as Yup from 'yup';
import ReCAPTCHA from 'react-google-recaptcha';

export const validationSchema = Yup.object().shape({
  name: Yup.string().required('Veuillez entrer un nom.'),
  email: Yup.string().email('Veuillez entrer un email valide.').required('Veuillez entrer un email.'),
  subject: Yup.string().required('Veuillez entrer un sujet.'),
  message: Yup.string().min(10, 'Veuillez entrer un message plus detaillé.').required('Veuillez entrer un message.'),
});

const useStyles = makeStyles((theme) => ({
  form: {
    width: '80%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
  flexSS: {
    display: 'flex',
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
    flex: 1,
    flexDirection: 'column',
  },
  field: {
    marginTop: '15px',
    width: '100% !important',
  },
  formfield: {
    width: '100% !important',
    display: 'inline-block',
    WebkitFontSmoothing: 'antialiased',
    minHeight: '40px !important',
    maxHeight: '150px !important',
    marginBottom: '15px',
    fontSize: '14px',
    lineHeight: '25px',
    color: '#73879c',
    resize: 'none',
    verticalAlign: 'middle',
    WebkitBoxShadow: 'none',
    MozBoxShadow: 'none',
    boxShadow: 'none',
    backgroundColor: '#f5f5f5',
    border: '1px solid #e6e9ed',
    WebkitBorderRadius: '0',
    borderRadius: '0',
    transition: 'all 200ms ease-out',
    WebkitTransition: 'all 200ms ease-out',
    OTransition: 'all 200ms ease-out',
    MozTransition: 'all 200ms ease-out',
  },
  error: {
    textAlign: 'left !important',
    display: 'inline-block',
    float: 'left',
    width: '100% !important',
    fontSize: '13px',
    clear: 'both',
    border: 'none !important',
    color: '#d99898',
    padding: '0 !important',
    marginTop: '-10px',
    marginBottom: '5px',
    maxWidth: '100%',
    fontWeight: '700',
  },
  msgsuccess: {
    color: '#237810 !important',
  },
  msgerror: {
    color: '#d99898 !important',
  },
}));

export default function ContactForm(props) {
  const classes = useStyles();

  const [captchaValue, setCaptchaValue] = useState(false);

  const [isSubmitted, setIsSubmitted] = useState(false);
  const [msgStatus, setMsgStatus] = useState(null);

  // display error under field like "required" or "too short"
  function displayError(fieldName, touched, errors) {
    if (touched[fieldName] && errors[fieldName]) {
      return <div className={classes.error}>{errors[fieldName]}</div>;
    } else {
      return null;
    }
  }

  // display success or fail after user click on send
  function renderMsgStatus() {
    if (msgStatus === null) {
      return null;
    }
    if (msgStatus?.success) {
      return <p className={clsx(classes.error, classes.msgsuccess)}>{msgStatus?.data?.msg}</p>;
    } else {
      return <p className={clsx(classes.error, classes.msgerror)}>{msgStatus?.data?.msg}</p>;
    }
  }

  function onChange(value) {
    setCaptchaValue(value);
  }

  return (
    <>
      <Formik
        initialValues={{
          name: '',
          email: '',
          subject: '',
          message: '',
        }}
        validationSchema={validationSchema}
        validateOnChange={true}
        validateOnBlur={true}
        onSubmit={async (values) => {
          let res = await sendMessage(values, captchaValue);
          setMsgStatus(res);
          if (res.status === 200) {
            setIsSubmitted(true);
          }
        }}
      >
        {({ touched, errors }) => {
          return (
            <Form className={clsx(classes.flexSS, classes.form)} noValidate>
              <div className={classes.field}>
                <Field name="name" placeholder="Nom" className={classes.formfield} />
                {displayError('name', touched, errors)}
              </div>
              <div className={classes.field}>
                <Field name="email" placeholder="Email" className={classes.formfield} />
                {displayError('email', touched, errors)}
              </div>
              <div className={classes.field}>
                <Field name="subject" placeholder="Sujet" className={classes.formfield} />
                {displayError('subject', touched, errors)}
              </div>
              <div className={classes.field}>
                <Field name="message" component="textarea" rows="4" placeholder="Votre message ..." className={classes.formfield} />
                {displayError('message', touched, errors)}
              </div>
              <ReCAPTCHA sitekey="6Lckn-MZAAAAAFKG78CcF4CK-BiD6A80o6SDlDoN" onChange={onChange} />
              <OrangeButton type="submit" disabled={!captchaValue || isSubmitted}>
                Envoyer
              </OrangeButton>
              {renderMsgStatus()}
            </Form>
          );
        }}
      </Formik>
    </>
  );
}

/**
 * envoi les données du message à l'API
 */
async function sendMessage(values, captchaValue) {
  console.log('sendMessage', values);
  const url = '/sendmessage';
  let status, data;
  try {
    const response = await axios.instance.post(url, { ...values, captchaValue }, axios.postConfig);
    status = response.status;
    data = response.data;
  } catch (error) {
    console.error(error);
    status = 404;
  }
  return { status, data };
}
