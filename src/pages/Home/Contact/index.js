import React from 'react';
import Grid from '@material-ui/core/Grid';
import clsx from 'clsx';
import {makeStyles} from '@material-ui/core/styles';
import PhoneAndroidIcon from '@material-ui/icons/PhoneAndroid';
import LocationOnIcon from '@material-ui/icons/LocationOn';
import MailOutlineIcon from '@material-ui/icons/MailOutline';
import TwitterIcon from '@material-ui/icons/Twitter';

import globalStyles from '../../../globalStyles';
import SectionContent from '../../../components/SectionContent';
import useResponsive from '../../../utilities/useResponsive';
import useGridClasses from '../../../utilities/useGridClasses';

import Config from '../../../Config';
import ContactForm from './ContactForm';

const useStyles = makeStyles((theme) => ({
  pt15: {
    paddingTop: '15px',
  },
  title: {},
  colorDB: {color: Config.colors.darkBlue},
}));

// : keybase://team/mnemotix.devops/mailgun/smtp.config
// https://www.npmjs.com/package/xss
// https://blog.mailtrap.io/react-send-email/

export default function RealisationsRecentes() {
  const classes = useStyles();
  const gS = globalStyles();
  const {isMobile} = useResponsive();
  const {grid, parentClassName} = useGridClasses();

  return (
    <SectionContent>
      <div className={clsx(parentClassName, gS.textJustify, gS.width100)}>
        {/** ajoute  {...grid} juste pour avoir le meme spacing que dans les autres grid */}
        <Grid container {...grid} direction="row" justifyContent="center" alignItems="stretch" spacing={10}>
          <Grid item xs={12} md={6} className={gS.width100}>
            <h2>Laissez-nous un message</h2>
            <ContactForm />
          </Grid>

          <Grid item xs={12} md={6} className={gS.width100}>
            <h2>Contacts</h2>

            <Grid container direction="row" justifyContent="flex-start" alignItems="flex-start" spacing={1} className={classes.pt15}>
              <Grid item className={gS.flexCenter}>
                <PhoneAndroidIcon />
              </Grid>
              <Grid item className={gS.flexStart}>
                +33 6 16 22 71 18
              </Grid>
            </Grid>

            <Grid container direction="row" justifyContent="flex-start" alignItems="flex-start" spacing={1} className={classes.pt15}>
              <Grid item className={gS.flexCenter}>
                <MailOutlineIcon />
              </Grid>
              <Grid item className={gS.flexStart}>
                <a href="mailto:contact&#64;mnemotix.com" className={gS.aNoDecoration}>
                  contact&#64;mnemotix.com
                </a>
              </Grid>
            </Grid>

            <Grid container direction="row" justifyContent="flex-start" alignItems="flex-start" spacing={1} className={classes.pt15}>
              <Grid item className={gS.flexCenter}>
                <LocationOnIcon />
              </Grid>
              <Grid item className={gS.flexStart}>
                <div>
                  Cité des Entreprises Bastide Rouge
                  <br />
                  11 Avenue Maurice Chevalier
                  <br />
                  06150 CANNES
                  <br />
                  FRANCE
                </div>
              </Grid>
            </Grid>

            <div className={classes.pt15}>
 
              <iframe
                title="google-maps"
                src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2891.655745479712!2d6.958296314303965!3d43.55121747912489!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x12ce82f77407b3dd%3A0x1ae13a35f51c7350!2sP%C3%A9pini%C3%A8re%20d&#39;entreprises%20Cr%C3%A9ACannes!5e0!3m2!1sfr!2sfr!4v1649056698837!5m2!1sfr!2sfr" 
                width={isMobile ? '260' : '460'}
                height={isMobile ? '240' : '300'}
                frameBorder="0"
                style={{border: 0}}
                allowFullScreen=""
                aria-hidden="false"
                tabIndex="0"
              ></iframe>
            </div>

            <h3>Suivez nous</h3>

            <a href="https://twitter.com/mnemotix" target="blank" className={clsx(classes.colorDB, gS.aNoDecoration)}>
              <TwitterIcon /> Twitter
            </a>
          </Grid>
        </Grid>
      </div>
    </SectionContent>
  );
}
