import React from 'react';
import globalStyles from '../../globalStyles';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import clsx from 'clsx';
import SectionContent from '../../components/SectionContent';
import Graph1 from '../../components/Animated/Graph1';
import Graph2 from '../../components/Animated/Graph2';
import Graph3 from '../../components/Animated/Graph3';
import ImportedGraphics from '../../ImportedGraphics';
import useGridClasses from '../../utilities/useGridClasses';

const useStyles = makeStyles((theme) => ({
  img: {
    width: '80%',
    maxWidth: '300px',
    height: 'auto',
    transition: 'transform 2000ms',
    //transform: 'scale(1.2) !important',

    '&:hover': {
      transition: 'transform 2000ms',
      transform: 'scale(1.2) !important',
    },
  },
}));

export default function Methodologie() {
  const classes = useStyles();
  const gS = globalStyles();
  const { grid, className, parentClassName } = useGridClasses();

  function renderGraph(index, useJPG = false) {
    switch (index) {
      case 1:
        if (useJPG) {
          return <img src={ImportedGraphics.illustrations_01} className={classes.img} alt="" />;
        } else {
          return <Graph1 />;
        }
      case 2:
        if (useJPG) {
          return <img src={ImportedGraphics.illustrations_02} className={classes.img} alt="" />;
        } else {
          return <Graph2 />;
        }
      case 3:
        if (useJPG) {
          console.log("use jpg 3")
          return <img src={ImportedGraphics.illustrations_03} className={classes.img} alt="" />;
        } else {
          return <Graph3 />;
        }
      default:
        if (useJPG) {
          return <img src={ImportedGraphics.illustrations_01} className={classes.img} alt="" />;
        } else {
          return <Graph1 />;
        }
    }
  }

  return (
    <SectionContent>
      <div className={gS.textJustify}>
        <Grid container {...grid} className={gS.p20}>
          <Grid item xs={12} md={6} className={className}>
            {renderGraph(1)}
          </Grid>
          <Grid item xs={12} md={6} className={className}>
            <h2>Vos données sont dispersées</h2>
            <p>Elles sont sur différents supports, dans différents formats et vous ne pouvez pas les exploiter.</p>
          </Grid>
        </Grid>

        <Grid container {...grid} className={gS.p20}>
          <Grid item xs={12} md={6} className={className}>
            {renderGraph(2)}
          </Grid>
          <Grid item xs={12} md={6} className={className}>
            <h2>Nous vous aidons à les structurer, les décloisonner et les lier</h2>
            <p>
              Quels que soient leur volume et leur complexité, nous tissons des liens entres les données afin qu’elles soient mieux valorisées et utilisées efficacement.
            </p>
          </Grid>
        </Grid>

        <Grid container {...grid} className={gS.p20}>
          <Grid item xs={12} md={6} className={className}>
            {renderGraph(3,true)}
          </Grid>
          <Grid item xs={12} md={6} className={className}>
            <h2>Vos données ont un sens et sont utiles</h2>
            <p>
              Nous vous proposons des solutions à forte valeur-ajoutée: cartographie, recommandation, calcul de métrique, dashboard, etc.
            </p>
          </Grid>
        </Grid>
      </div>
    </SectionContent>
  );
}
