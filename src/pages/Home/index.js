import React, { useEffect, useState } from 'react';
import { useDebounce } from 'use-debounce';
import { useHistory } from 'react-router-dom';
import WaypointSection from '../../components/WaypointSection';
import TopSlider from './TopSlider';
import SectionTitle from '../../components/SectionTitle';
import TestimonalsSlider from '../../components/TestimonalsSlider';

import Methodologie from './Methodologie';
import Knowledge from './Knowledge';
import Services from './Services';
import Apropos from './Apropos';
import NosValeurs from './NosValeurs';
import NotreEquipe from './NotreEquipe';
import NosPartenaires from './NosPartenaires';
import Confiance from './Confiance';
import RealisationsRecentes from './RealisationsRecentes';
import Recrutement from './Recrutement';
import Contact from './Contact';

export default function Home() {
  useEffect(() => {
    document.title = 'Donnez du sens à vos données.';
  }, []);

  const history = useHistory();
  /*
    let [hash, setHash] = useState(null);
    let [debouncedHash] = useDebounce(hash, 250);

    useEffect(() => {
      history.push('/#' + debouncedHash);
    }, [debouncedHash]);
      function handleCurrentAnchor(hash) {
    //setHash(hash);
    console.log('set', hash);
    history.push('/#' + debouncedHash);
  }
  */

  function handleCurrentAnchor(hash) {
    history.replace('/#' + hash);
  }
 
  return (
    <div>
      <TopSlider />
      <Knowledge />
      <WaypointSection setCurrentAnchor={handleCurrentAnchor} hash="methodologie">
        <SectionTitle color="blue" title="Notre méthodologie" />
        <Methodologie />
      </WaypointSection>

      <WaypointSection setCurrentAnchor={handleCurrentAnchor} hash="services">
        <SectionTitle color="orange" title="Nos services" />
        <Services />
      </WaypointSection>

      <WaypointSection setCurrentAnchor={handleCurrentAnchor} hash="apropos">
        <SectionTitle color="blue" title="Mnemotix" />
        <Apropos />
      </WaypointSection>
      <NosValeurs />

      <NotreEquipe />
      <NosPartenaires />
      <WaypointSection setCurrentAnchor={handleCurrentAnchor} hash="clients">
        <TestimonalsSlider id="clients" />
      </WaypointSection>

      <Confiance />
      <WaypointSection setCurrentAnchor={handleCurrentAnchor} hash="realisations">
        <SectionTitle color="orange" title="Réalisations récentes" />
        <RealisationsRecentes />
      </WaypointSection>
      <WaypointSection setCurrentAnchor={handleCurrentAnchor} hash="recrutement">
        <SectionTitle color="blue" title="Recrutement" subTitle="Mnemotix a besoin de vos talents et recherche..." />
        <Recrutement />
      </WaypointSection>
      <WaypointSection setCurrentAnchor={handleCurrentAnchor} hash="contact">
        <SectionTitle color="orange" title="Contactez-nous" subTitle="Une équipe à votre service." />
        <Contact />
      </WaypointSection>

      <div style={{ color: 'white', fontSize: '25px' }}> V2.0.0</div>
    </div>
  );
}
