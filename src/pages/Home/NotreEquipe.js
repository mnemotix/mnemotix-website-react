import React from 'react';
import globalStyles from '../../globalStyles';
import clsx from 'clsx';
import Grid from '@material-ui/core/Grid';
import SectionContent from '../../components/SectionContent';
import useGridClasses from '../../utilities/useGridClasses';
import ImportedGraphics from '../../ImportedGraphics';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  name: {
    marginTop: '40px',
    fontSize: '26px',
    lineHeight: '130%',
    textAlign: 'center !important',
    fontWeight: 700,
    letterSpacing: '0.025em',
  },
  job: {
    fontSize: '18px',
    lineHeight: '140%',
    marginTop: '5px',
    fontWeight: 400,
    letterSpacing: '0.015em',
    textAlign: 'center !important',
  },
  root: {
    width: '100%',
  },
}));

export default function NotreEquipe() {
  const gS = globalStyles();
  const classes = useStyles();
  const { className } = useGridClasses();

  return (
    <SectionContent>
      <div className={gS.sectionTitle}>Notre équipe :</div>
      <div className={clsx(classes.root, gS.paddingT50)}>
        <Grid container direction="row" justifyContent="space-around" alignItems="stretch">
          <Grid item xs={12} md={4} className={className}>
            <img alt="" src={ImportedGraphics.team_ndelaforge} className={gS.img200} />
            <div className={classes.name}>Nicolas Delaforge</div>
            <div className={classes.job}>Co-fondateur, CEO & CTO</div>
          </Grid>
          <Grid item xs={12} md={4} className={className}>
            <img alt="" src={ImportedGraphics.team_mleitzelman} className={gS.img200} />
            <div className={classes.name}> Mylène Leitzelman</div>
            <div className={classes.job}>Co-fondatrice et Data scientist</div>
          </Grid>
          <Grid item xs={12} md={4} className={className}>
            <img alt="" src={ImportedGraphics.team_mrogelja} className={gS.img200} />
            <div className={classes.name}>Mathieu Rogelja</div>
            <div className={classes.job}>Responsable Front-end</div>
          </Grid>

          <Grid item xs={12} md={4} className={className}>
            <img alt="" src={ImportedGraphics.team_prlherisson} className={gS.img200} />
            <div className={classes.name}>Pierre-René Lherisson</div>
            <div className={classes.job}>Développeur Back-end</div>
          </Grid>
          <Grid item xs={12} md={4} className={className}>
            <img alt="" src={ImportedGraphics.team_aibrahim} className={gS.img200} />
            <div className={classes.name}>Alain Ibrahim</div>
            <div className={classes.job}>Développeur Front-end</div>
          </Grid>
          <Grid item xs={12} md={4} className={className}>
            <img alt="" src={ImportedGraphics.team_flimpens} className={gS.img200} />
            <div className={classes.name}>Freddy Limpens</div>
            <div className={classes.job}>Chef de projet</div>
          </Grid>
        </Grid>
      </div>
    </SectionContent>
  );
}
