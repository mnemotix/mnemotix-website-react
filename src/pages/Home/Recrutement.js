import React from 'react';
import globalStyles from '../../globalStyles';
import SectionContent from '../../components/SectionContent';
import { makeStyles } from '@material-ui/core/styles';
import Config from '../../Config';

const useStyles = makeStyles((theme) => ({
  orangeBorderButton: {
    display: 'inline-block',
    textAlign: 'center',
    color: Config.colors.orange,
    border: `4px solid ${Config.colors.orange}`,
    marginTop: '50px',
    padding: '25px',
  },
}));

export default function Recrutement() {
  const classes = useStyles();
  const gS = globalStyles();

  return (
    <SectionContent>
      <div className={gS.textAlignCenter}>
        {/*
        <div style="text-align: center">
            <div style="display: inline-block; text-align: left;">
              <ul>
                <li>
                  <h2>- Un-e développeur-se Front-End</h2>
                </li>
                <li>
                  <h2>- Un-e développeur-se Back-End</h2>
                </li>
                <li>
                  <h2>- Un-e chef-fe de projet</h2>
                </li>
              </ul> 
            </div>
          </div>
        */}
        <div>
          <a href="mailto:contact&#64;mnemotix.com?subject=candidature spontannée">
            <h2 className={classes.orangeBorderButton}>Vous souhaitez déposer une candidature spontannée ?</h2>
          </a>
        </div>
      </div>
    </SectionContent>
  );
}
