import React from 'react';
import globalStyles from '../../globalStyles';
import clsx from 'clsx';

import { Grid } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
//import FavoriteBorderIcon from '@material-ui/icons/FavoriteBorder';
import ShareIcon from '@material-ui/icons/Share';
import TransformIcon from '@material-ui/icons/Transform';
import BrightnessLowIcon from '@material-ui/icons/BrightnessLow';

import SectionContent from '../../components/SectionContent';
import Config from '../../Config';
import useGridClasses from '../../utilities/useGridClasses';

const useStyles = makeStyles((theme) => ({
  backgrd: {
    width: '100%',
    backgroundColor: `${Config.colors.orangeLight} !important`,
    color: Config.colors.darkBlue,
  },
  orangeColor: {
    color: Config.colors.orange,
  },
}));

export default function NosValeurs() {
  const gS = globalStyles();
  const classes = useStyles();
  const { grid, className, parentClassName } = useGridClasses();
  return (
    <div className={classes.backgrd}>
      <SectionContent nobackground={true}>
        <div className={gS.sectionTitle}>Nos valeurs</div>
        <div className={clsx(gS.sectionSubTitle, gS.textJustify, gS.p20)}>
          Nous croyons au fort potentiel du numérique et du Web en particulier, pour construire la société de demain où les savoirs seront
          décloisonnés et uniformément partagés.
        </div>

        <div className={clsx(parentClassName, gS.textJustify, gS.paddingT50)}>
          <Grid container {...grid}>
            <Grid item xs={12} md={4} className={className}>
              <ShareIcon className={clsx(gS.img100, classes.orangeColor)} />
              <h2>Esprit coopératif</h2>
              <p>
                Nous croyons à la synergie des compétences et au fonctionnement en réseau plutôt qu'à la centralisation du savoir-faire.
                <br />
                Notre ambition est de fédérer un ensemble d'acteurs du numérique qui partagent notre vision.
                <br />
                Notre statut de{' '}
                <a
                  className={gS.aUnderline}
                  href="http://www.economie.gouv.fr/cedef/economie-sociale-et-solidaire"
                  target="_blank"
                  rel="noreferrer"
                >
                  SCIC
                </a>{' '}
                aime à aller dans ce sens.
              </p>
            </Grid>
            <Grid item xs={12} md={4} className={className}>
              <TransformIcon className={clsx(gS.img100, classes.orangeColor)} />
              <h2>Transfert d'innovation</h2>
              <p>
                Venant du monde de la recherche, nous préservons un lien étroit avec de prestigieux laboratoires.
                <br />
                Nos développements intègrent des briques logicielles issues de la recherche que nous participons activement à rendre plus
                stables.
                <br />
                Cette approche permet à nos clients de bénéficier des dernières innovations technologiques, avantage concurrentiel non
                négligeable. De l'autre côté, les chercheurs bénéficient de retours utiles pour faire avancer leurs projets.{' '}
              </p>
            </Grid>
            <Grid item xs={12} md={4} className={className}>
              <BrightnessLowIcon className={clsx(gS.img100, classes.orangeColor)} />
              <h2>Acteur de(s) transition(s)</h2>
              <p>
                Nous sommes convaincus du rôle des nouvelles technologies et du Web comme facilitateur dans le processus de changement de
                notre société et de nos modes de consommation.
                <br />
                Nous nous engageons donc aux côtés des acteurs de la transition énergétique, sociale et écologique notamment en les
                assistant dans l'organisation de leurs communautés.
              </p>
            </Grid>
          </Grid>
        </div>
      </SectionContent>
    </div>
  );
}
