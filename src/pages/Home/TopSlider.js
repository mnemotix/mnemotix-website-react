import React, {useState} from 'react';
import Slider from '@farbenmeer/react-spring-slider';
import {makeStyles} from '@material-ui/core/styles';
import {Grid} from '@material-ui/core';
import Config from '../../Config';
import clsx from 'clsx';
import ImportedGraphics from '../../ImportedGraphics';
import useResponsive from '../../utilities/useResponsive';
import globalStyles from '../../globalStyles';
import Banner from '../../components/Banner';
import OrangeButton from '../../components/OrangeButton';

const useStyles = makeStyles((theme) => ({
  root: {
    height:'min(600px,100vh) !important ',
    [theme.breakpoints.down('sm')]: {
      height: 'min(80vh, 800px) !important',
    },
    width: '100%',
  },
  centerSmallScreen: {
    [theme.breakpoints.down('md')]: {
      display: 'flex',
      flexDirection: 'column',
      alignItems: 'center',
      justifyContent: 'center',
      flex: 1,
    },
  },
  sectionDesktop: {
    display: 'none',
    [theme.breakpoints.up('md')]: {
      display: 'flex'
    },
  },
  sectionMobile: {
    display: 'flex',
    [theme.breakpoints.up('md')]: {
      display: 'none',
    },
  },
  ddfMainText: {
    textAlign: 'left',
    padding: '20px',
    lineHeight: '120%',
    fontSize: 'calc(6px + 5vmin)!important',
    fontWeight: 'bold',
  },
  mainText: {
    textAlign: 'left',
    padding: '20px',
    lineHeight: '120%',
    fontSize: 'calc(10px + 5vmin)!important',
    fontWeight: 'bold',
  },
  marginRight25: {
    marginRight: '25px',
  },
  weeverMainText: {
    textAlign: 'right',
    lineHeight: '110%',

    [theme.breakpoints.down('sm')]: {
      textAlign: 'center',
    },
    fontSize: 'calc(10px + 4vmin) !important',  
    /*
    [theme.breakpoints.down('sm')]: {
      textAlign: 'center',
      fontSize: 'calc(10px + 4vmin) !important'
    },
    [theme.breakpoints.up('sm')]: {
      marginTop: '25px',
      fontSize: 'calc(10px + 2vmin) !important',
    },
    [theme.breakpoints.up('md')]: {
      fontSize: 'calc(10px + 3vmin) !important',
    },
    [theme.breakpoints.up('lg')]: {
      fontSize: 'calc(10px + 4vmin) !important',
    },*/
    fontWeight: 'bold',
    color: Config.colors.darkBlue,
  },
  weeverLogo: {
    width: 'min(30vw,400px)',
    height: 'auto',
    [theme.breakpoints.down('sm')]: {
      width: '250px'
    },
  },
  weeverRightLogo: {
    width: "min(100px,9vw)",
    height: "auto",
    margin: '10px'
  },
  leftM20: {
    marginLeft: "20px"
  }
}));

/**
 * affiche le slider du haut avec qlq projets en plus comme mnb et ddf
 */
export default function TopSlider() {
  const classes = useStyles();
  const gS = globalStyles();
  const {isDesktop} = useResponsive();
  const _initalTimer = 7000;
  const [sliderTimer, setSliderTimer] = useState(_initalTimer);

  const BulletComponent = ({onClick, isActive}) => {
    return (
      <li
        style={{
          width: 'min(25px, 3vmin)',
          height: 'min(25px, 3vmin)',
          backgroundColor: 'white',
          margin: '0',
          padding: '12px 1px 0px 6px',
        }}
        onClick={(params) => {
          onClick(params);
          setSliderTimer(0);
          setTimeout(() => {
            setSliderTimer(_initalTimer);
          }, 30000);
        }}
      >
        <div
          style={{
            width: '60%',
            height: '60%',
            margin: 'auto',
            backgroundColor: isActive ? Config.colors.orange : Config.colors.darkBlue,
            borderRadius: '50px',
          }}
        />
      </li>
    );
  };

  const componentsToRender = [
    <Banner
      key={1}
      style={{
        backgroundImage: `url(${ImportedGraphics.background_slider_graph})`,
      }}
    >
      <div className={clsx(classes.mainText, gS.lightColor)}>
        Nous structurons et relions vos données pour les rendre intelligentes qu'importent leur volume et leur complexité
        {/*  Nous structurons et relions vos données pour les rendre intelligentes quels que soient leur volume et leur complexité*/}
      </div>
    </Banner>,

    <Banner
      key={2}
      style={{
        backgroundImage: `url(${ImportedGraphics.mnb_slider_back})`,
      }}
    >
      <Grid container direction="row" justifyContent="center" alignItems="center">
        <Grid item xs={10} md={6} className={clsx(classes.mainText, gS.lightColor, isDesktop ? gS.flexEnd : gS.flexCenter)}>
          <div className={clsx(isDesktop && classes.marginRight25)}>
            BioMétéo, <br />
            la biodiversité <br />
            près de chez vous
            <div>
              <OrangeButton href="/mnb">Découvrez le projet</OrangeButton>
            </div>
          </div>
        </Grid>
        <Grid item xs={10} md={6} className={clsx(isDesktop ? gS.flexStart : gS.flexCenter)}>
          <img src={ImportedGraphics.mnb_ordi} alt="" className={gS.responsiveImageW80} />
        </Grid>
      </Grid>
    </Banner>,

    <Banner
      key={2}
      style={{
        backgroundImage: `url(${ImportedGraphics.background_slider_graph})`,
      }}
    >
      <div className={clsx(classes.mainText, gS.lightColor)}>
        Optez pour du sur-mesure
        <p className={gS.subText}>Nos solutions personnalisées sont adaptées à la complexité de vos besoins</p>
        <OrangeButton href="#services">Voir nos services</OrangeButton>
      </div>
    </Banner>,
    <Banner
      key={4}
      style={{
        backgroundColor: Config.colors.creme,
        backgroundImage: `url(${ImportedGraphics.ddf_slider_back})`,
      }}
    >
      <div className={clsx(gS.flexEnd, gS.flexDirectionCol)}>
        {/* la mise en forme est trop différente entre mobile et desktop utiliser Hidden bug, ver 800px de large aucun des deux ne s'affiche 
          !!!!!!!!!!!!!
          !
          !
          !      sectionMobile  vs   sectionDesktop   = cache la div selon le device via css
          !
          !
          !!!!!!!!!!!!!
          */}
        {/*  sectionMobile */}
        <Grid
          container
          direction="column"
          justifyContent="flex-end"
          alignItems="center"
          className={clsx(classes.sectionMobile, classes.ddfMainText, gS.darkBlueColor)}
        >
          <Grid item className={gS.textAlignCenter}>
            Mnemotix est fier de présenter le dictionnaire des francophones !
            </Grid>
          <Grid item className={clsx(gS.subText, gS.textAlignCenter)}>
            Un outil collaboratif pour 300 millions de personnes parlant le français autour du monde
            </Grid>
          <Grid item>
            <OrangeButton href="/ddf">Découvrez le projet</OrangeButton>
          </Grid>
          <Grid item xs={10} md={6} className={clsx(gS.flexEnd, gS.flexDirectionCol)}>
            <img src={ImportedGraphics.ddf_smartphone} alt="" className={gS.responsiveImageW80} />
          </Grid>
        </Grid>
        {/*  sectionDesktop */}
        <Grid container direction="row" justifyContent="center" alignItems="stretch" className={classes.sectionDesktop}>
          <Grid item xs={10} md={4} className={clsx(gS.flexCenter, gS.flexDirectionCol)}>
            <img src={ImportedGraphics.ddf_smartphone} alt="" className={gS.responsiveImageH80} />
          </Grid>
          <Grid item xs={10} md={8} className={clsx(gS.flexCenter, classes.ddfMainText, gS.darkBlueColor)}>
            <Grid container direction="column" justifyContent="center" alignItems="flex-end">
              <Grid item className={gS.textAlignRight}>
                Mnemotix est fier de présenter le dictionnaire des francophones !
                </Grid>
              <Grid item className={clsx(gS.subText, gS.textAlignRight)}>
                Un outil collaboratif pour 300 millions de personnes parlant le français autour du monde
                </Grid>
              <Grid item>
                <OrangeButton target="_blank" href="/ddf">Découvrez le projet</OrangeButton>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </div>
    </Banner>,  
    <Banner
      key={5}
      style={{
        backgroundColor: Config.colors.weeverGrey,
        backgroundImage: `url(${ImportedGraphics.weever.weever_slider_back})`,
      }}
    >
      <div className={clsx(gS.flexCenter, !isDesktop && gS.flexDirectionCol)}>

        <img src={ImportedGraphics.weever.weever_column_logo} alt="weever" className={classes.weeverLogo} />

        <div className={clsx(gS.lightColor, isDesktop ? gS.flexEnd : gS.flexCenter)}>
          <div className={clsx(classes.weeverMainText)}>
            {isDesktop ?
              <>Weever, du document<br />à la connaissance<br />partageable et<br />actionnable</>
              :
              <div><br/><br/>Weever, du document à la connaissance partageable et actionnable</div>
            }
            <div>
              <OrangeButton href="https://weever-website.mnemotix.com/">Découvrez le projet</OrangeButton>
            </div>
          </div>
        </div>
        {isDesktop &&
          <div className={clsx(gS.flexCenter, gS.flexDirectionCol, classes.leftM20)}>
            <img src={ImportedGraphics.weever.weever_icon_a} alt="" className={classes.weeverRightLogo} />
            <img src={ImportedGraphics.weever.weever_icon_b} alt="" className={classes.weeverRightLogo} />
            <img src={ImportedGraphics.weever.weever_icon_c} alt="" className={classes.weeverRightLogo} />
            <img src={ImportedGraphics.weever.weever_icon_d} alt="" className={classes.weeverRightLogo} />
          </div>
        }

      </div>
    </Banner >,
  ]


  return (
    <div className={classes.root}>
      <Slider hasBullets BulletComponent={BulletComponent} auto={sliderTimer} style={{margin: '0px !important'}}>
        {componentsToRender}
      </Slider>      
    </div>
  );
}
