import React from 'react';
import globalStyles from '../../globalStyles';
import clsx from 'clsx';
import Grid from '@material-ui/core/Grid';
import SectionContent from '../../components/SectionContent';
import useGridClasses from '../../utilities/useGridClasses';
import Tooltip from '@material-ui/core/Tooltip';
import ImportedGraphics from '../../ImportedGraphics';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  img: {
    borderRadius: '10%',
    width: '80%',
    height: 'auto',
    maxWidth: '200px',
    verticalAlign: 'middle',
  },
}));

const tooltipMS = makeStyles((theme) => ({
  arrow: {
    color: theme.palette.common.black,
  },
  tooltip: {
    backgroundColor: theme.palette.common.black,
    fontSize: 16,
  },
}));

export default function Partners() {
  const gS = globalStyles();
  const classes = useStyles();
  const tooltipClasses = tooltipMS();
  const { className } = useGridClasses();

  const clients = [
    { name: 'ADEME', src: ImportedGraphics.clients_ademe },
    { name: 'The Green Communication', src: ImportedGraphics.clients_tgc },
    { name: 'Fondation des Galeries Lafayette', src: ImportedGraphics.clients_fegl },
    { name: 'CNRS', src: ImportedGraphics.clients_cnrs },
    { name: 'INRIA', src: ImportedGraphics.clients_inria },
    { name: 'SICTIAM', src: ImportedGraphics.clients_sictiam },
    { name: 'Infotel', src: ImportedGraphics.clients_infotel },
    { name: 'Groupe PSA', src: ImportedGraphics.clients_psa },
    { name: 'MindMatcher', src: ImportedGraphics.clients_mindmatcher },
    { name: '1D Lab', src: ImportedGraphics.clients_1dlab },
    { name: 'ISTHIA', src: ImportedGraphics.clients_isthia },
    { name: 'Villa Arson Nice', src: ImportedGraphics.clients_va },
    { name: "Université Côte d'Azur", src: ImportedGraphics.clients_uca },
    { name: 'Université Toulouse Jean Jaurès', src: ImportedGraphics.clients_ujjt },
    { name: 'Ministère de la culture et la communication', src: ImportedGraphics.clients_ministereculture },
    { name: 'Educlever', src: ImportedGraphics.clients_educlever },
    { name: 'CoreKap', src: ImportedGraphics.clients_corekap },
    { name: 'Clairsienne', src: ImportedGraphics.clients_clairsienne },
    { name: 'UTC', src: ImportedGraphics.clients_utc },
    { name: '2IF', src: ImportedGraphics.clients_2if },
  ];
  return (
    <SectionContent>
      <div className={gS.sectionTitle}>Ils nous ont fait confiance :</div>

      <div className={clsx(classes.root, gS.paddingT50)}>
        <Grid container direction="row" justifyContent="space-evenly" alignItems="stretch">
          {clients.map((client, index) => {
            return (
              <Grid key={index} item xs={6} md={3} className={className}>
                <Tooltip title={client.name} arrow classes={tooltipClasses}>
                  <img src={client.src} className={classes.img} alt="" />
                </Tooltip>
              </Grid>
            );
          })}
        </Grid>
      </div>
    </SectionContent>
  );
}
