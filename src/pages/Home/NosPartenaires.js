import React from 'react';
import globalStyles from '../../globalStyles';
import clsx from 'clsx';
import Grid from '@material-ui/core/Grid';
import SectionContent from '../../components/SectionContent';
import useGridClasses from '../../utilities/useGridClasses';
import ImportedGraphics from '../../ImportedGraphics';

import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
  },
  img350: {
    width: '350px',
    maxWidth: '80vw',
    borderRadius: '10%',
  },
}));

export default function NosPartenaires() {
  const gS = globalStyles();
  const classes = useStyles();
  const { className } = useGridClasses();

  return (
    <SectionContent>
      <div className={gS.sectionTitle}>Nos partenaires :</div>
      <div className={clsx(classes.root, gS.paddingT50)}>
        <Grid container direction="row" justifyContent="space-evenly" alignItems="stretch">
          <Grid item xs={12} md={4} className={className}>
            <a href="https://www.devops.works/" target="_blank" rel="noreferrer">
              <img src={ImportedGraphics.partners_devops} className={classes.img350} alt="" />
            </a>
          </Grid>
          <Grid item xs={12} md={4} className={className}>
            <a href="https://www.elzeard.co/" target="_blank" rel="noreferrer">
              <img src={ImportedGraphics.partners_elzeard} className={classes.img350} alt="" />
            </a>
          </Grid>
        </Grid>
        <Grid container direction="row" justifyContent="space-evenly" alignItems="stretch">
          <Grid item xs={12} md={4} className={className}>
            <a href="https://www.ontotext.com/" target="_blank" rel="noreferrer">
              <img src={ImportedGraphics.partners_ontotext} className={classes.img350} alt="" />
            </a>
          </Grid>
          <Grid item xs={12} md={4} className={className}>
            <a href="https://team.inria.fr/wimmics/" target="_blank" rel="noreferrer">
              <img src={ImportedGraphics.partners_winmics} className={classes.img350} alt="" />
            </a>
          </Grid>
        </Grid>
      </div>
    </SectionContent>
  );
}
