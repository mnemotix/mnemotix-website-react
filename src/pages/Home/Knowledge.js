import React from 'react';
import globalStyles from '../../globalStyles';
import clsx from 'clsx';
import SectionContent from '../../components/SectionContent';
import ImportedGraphics from '../../ImportedGraphics';

export default function Services() {
  const gS = globalStyles();

  return (
    <SectionContent>
      <h1 className={gS.sc_titleUpper}>
        <div className={gS.orangeBackgroundColor}>Grâce au knowledge graph</div>
      </h1>

      <div className={clsx(gS.flexCenter, gS.flexDirectionCol, gS.paddingT35)}>
        <img src={ImportedGraphics.art_croix} className={gS.img50} alt="" />

        <h2 className={gS.textAlignCenter}>Renforcez l'intelligence collective de votre organisation</h2>
      </div>

      <div className={clsx(gS.flexCenter, gS.flexDirectionCol, gS.paddingT35)}>
        <img src={ImportedGraphics.art_carre} className={gS.img50} alt="" />
        <h2 className={gS.textAlignCenter}>Mesurez l'impact de votre activité</h2>
      </div>

      <div className={clsx(gS.flexCenter, gS.flexDirectionCol, gS.paddingT35)}>
        <img src={ImportedGraphics.art_triangle} className={gS.img50} alt="" />
        <h2 className={gS.textAlignCenter}>Mobilisez les bonnes ressources au bon moment </h2>
      </div>
    </SectionContent>
  );
}
