import React from 'react';
import ReactDOM from 'react-dom';

import Graph1 from '../components/Animated/Graph1';

import Graph3 from '../components/Animated/Graph3';
import Graph2 from '../components/Animated/Graph2';

import { makeStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import clsx from 'clsx';

const useStyles = makeStyles((theme) => ({
  animatedItem: {
    animation: `$myEffect 3000ms ${theme.transitions.easing.easeInOut}`,
  },

  '@keyframes myEffect': {
    '0%': {
      transform: 'translateY(-200%)',
    },
    '100%': {
      transform: 'translateY(0)',
    },
  },
}));

export default function Test() {
  const classes = useStyles();
  return (
    <>
      <Graph3 />
    </>
  );
}
/*

  <div className={clsx(classes.animatedItem)}>
        <h1>Hello CodeSandbox</h1>
        <h2>Start editing to see some magic happen!</h2>
      </div>
      */
