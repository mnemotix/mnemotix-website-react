import React, {useEffect} from 'react';
import {Grid} from '@material-ui/core';
import clsx from 'clsx';
import Banner from '../components/Banner';
import SectionContent from '../components/SectionContent';
import CircleText from '../components/CircleText';
import Config from '../Config';
import globalStyles from '../globalStyles';
import ImportedGraphics from '../ImportedGraphics';

export default function Ddf() {
  const gS = globalStyles();

  useEffect(() => {
    document.title = 'Dictionnaire des francophones.';
  }, []);



  function renderListEntry(text, img) {
    return (<div className={gS.pages_marginH35v25}>
      <Grid container direction="row" justifyContent="flex-start" alignItems="flex-start" spacing={1}>
        <Grid item xs={2} sm={1} className={gS.pages_mCenter}>
          <img src={img} className={gS.pages_img50} alt="" />
        </Grid>
        <Grid item xs={10} sm={11} className={gS.pages_mCenter}>
          {text}
        </Grid>
      </Grid>
    </div>)
  }

  return (
    <div className={clsx(gS.flexStart, gS.flexDirectionCol, gS.width100)}>
      <Banner
        style={{
          backgroundColor: Config.colors.creme,
          backgroundImage: `url(${ImportedGraphics.ddf_slider_back})`,
        }}
      >
        <div className={gS.flexEnd}>
          <img src={ImportedGraphics.ddf_tabtext} className={gS.pages_mainImg} alt="" />
        </div>
      </Banner>

      <SectionContent>
        <div className={gS.width100}>
          <div className={gS.paddingTB25}>
            <h2 className={gS.pages_darkblueBgColor}>Le dictionnaire des francophones </h2>
            <div className={gS.pages_marginH35v25}>
              Le Dictionnaire des francophones (disponible sur{' '}
              <a href="https://www.dictionnairedesfrancophones.org/" target="_blank" className={gS.ahref}>
                www.dictionnairedesfrancophones.org
              </a>
              ) est un dictionnaire collaboratif numérique et open source qui a pour objectif de rendre compte de la richesse du français parlé au sein de l'espace francophone. C'est un projet institutionnel novateur qui présente d'une part une partie de consultation au sein de laquelle sont compilées plusieurs ressources lexicographiques (avec plus de 440 000 mots et 610 000 définitions), et d'autre part une partie collaborative pour développer les mots et faire vivre la langue française.
            </div>
            <div className={clsx(gS.pages_marginH35v25, gS.pages_videoResponsive)}>
              <iframe title='video' width="560" height="315"
                src="https://www.youtube.com/embed/kDEsFJvJkpk" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen
                style={{
                  left: '0',
                  top: '0',
                  height: '100%',
                  width: '100%',
                  position: 'absolute',
                }}
              ></iframe>
            </div>
          </div>

          <div className={gS.paddingTB25}>
            <h2 className={gS.pages_darkblueBgColor}>Les clients </h2>
            <div className={gS.pages_marginH35v25}>
              Porté par l'Institut international pour la francophonie (2IF) de l'Université Lyon 3 Jean Moulin, le projet de Dictionnaire des francophones s'inscrit dans un partenariat entre la Délégation Générale à la Langue Française et aux Langues de France du ministère de la Culture, le ministère de l'Europe et des Affaires étrangères, l'Institut français, l'Organisation internationale de la Francophonie (OIF), l'Académie des sciences d'Outre-Mer (ASOM), TV5MONDE et l'Agence universitaire de la Francophonie (AUF).<br /><br />

            Parmi les mesures du plan d'action « Une ambition pour la langue française et le plurilinguisme » du Président de la République, le Dictionnaire des francophones est un ouvrage inédit et singulier, articulant plusieurs ressources lexicographiques décrivant le français. Il combine à la fois une solide ambition scientifique et une démarche collective participative.
            </div>
          </div>

          <div className={gS.paddingTB25}>
            <h2 className={gS.pages_darkblueBgColor}>Les objectifs</h2>

            {/* <Grid container direction="row" justifyContent="center" alignItems="center">
                <Grid item xs={12} md={6} className={clsx(gS.flexCenter, gS.paddingB50)}>
                  <CircleText>Illustrer et faire vivre la richesse du français dans le monde.</CircleText>
                </Grid>
                <Grid item xs={12} md={6} className={clsx(gS.flexCenter, gS.paddingB50)}>
                  <CircleText secondColor>
                    Créer un dictionnaire singulier qui met sur un pied d'égalité l'ensemble des mots du français parlé dans l'ensemble de
                    la francophonie, de manière descriptive, non normative.
                  </CircleText>
                </Grid>
                </Grid>*/}
            {renderListEntry("Illustrer et faire vivre la richesse du français dans le monde.", ImportedGraphics.art_carreorange)}
            {renderListEntry("Créer un dictionnaire singulier qui met sur un pied d'égalité l'ensemble des mots du français parlé dans l'ensemble de la francophonie, de manière descriptive, non normative.", ImportedGraphics.art_triangleorange)}


          </div>

          <div className={gS.paddingTB25}>
            <h2 className={gS.pages_darkblueBgColor}>Les détails de la prestation</h2>
            {renderListEntry("Structuration d'une base de connaissance à partir de notre socle technologique open source Synaptix", ImportedGraphics.art_carreorange)}
            {renderListEntry("Importation et structuration de dictionnaires et de bases de mots, dont le Wiktionnaire", ImportedGraphics.art_triangleorange)}
            {renderListEntry("Alignement des concepts des dictionnaires analysés autour d'une ontologie pivot", ImportedGraphics.art_carreorange)}
            {renderListEntry("Réalisation d'une application web et mobile permettant 1) la recherche et la consultation des mots et définitions du DDF (intégrant la recherche sémantiquement contextualisée et géolocalisée), et 2) la contribution collaborative via l'ajout de nouvelles définitions ou d'informations structurées complétant le contenu existant", ImportedGraphics.art_triangleorange)}
          </div>

          <div className={gS.paddingTB25}>
            <h2 className={gS.pages_darkblueBgColor}>Visualisations de données</h2>

            <div className={gS.pages_marginH35v25}>
              <i>
                Les formes ou les définitions peuvent être associés à une localité dans le DDF. La carte ci-dessous représente pour chaque
                localité du monde francophone (pays, région, ville, etc.) le nombre et la liste des mots qui y sont rattachés.
              </i>
            </div>

            <div className={clsx(gS.pages_marginH35v25, gS.pages_videoResponsive)}>
              <iframe
                title="DDF - Répartitions des mots ayant un sens localisé"
                aria-label="World Symbol map"
                id="datawrapper-chart-K5Ezm"
                src="//datawrapper.dwcdn.net/K5Ezm/2/"
                scrolling="no"
                frameBorder="0"
                style={{
                  left: '0',
                  top: '0',
                  height: '100%',
                  width: '100%',
                  position: 'absolute',
                }}
                height="400"
              ></iframe>
            </div>
          </div>
        </div>
      </SectionContent>
    </div>
  );
}
