import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';

import { makeStyles, createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import reportWebVitals from './reportWebVitals';
import { BrowserRouter } from 'react-router-dom';
import NavHeader from './components/NavHeader';
import Footer from './components/Footer';
import Config from './Config';
import { Switch, Route } from 'react-router-dom';
import Home from './pages/Home/index';
import Mnb from './pages/Mnb';
import Test from './pages/Test';
import Ddf from './pages/Ddf';
import ScrollToTop from './components/ScrollToTop';

const theme = createMuiTheme({
  breakpoints: {
    values: {
      xs: 0,
      sm: 600,
      md: 960,
      lg: 1380,
      xl: 1920,
    },
  },
});

const useStyles = makeStyles((theme) => ({
  app: {
    fontSize: 'calc(10px + 4vmin)',
    display: 'flex',
    height: '100vh',
    justifyContent: 'space-between',
    flexDirection: 'column',
    backgroundColor: Config.colors.white,
    color: Config.colors.darkBlue,
  },
  width100: {
    width: '100%',
  },
  content: {
    display: 'flex',
    flex: '1',
    backgroundColor: Config.colors.white,
    marginTop: Config.navbar.height,
  },
}));

function App() {
  const classes = useStyles();

  return (
    <MuiThemeProvider theme={theme}>
      <div className={classes.app}>
        <NavHeader />
        <div className={classes.content} id="back-to-top-anchor">
          <div className={classes.width100}>
            <Switch>
              <Route path="/test" component={Test} />
              <Route path="/mnb" component={Mnb} />
              <Route path="/ddf" component={Ddf} />
              <Route component={Home} />
            </Switch>

            <Footer />
          </div>
        </div>

        <ScrollToTop id="back-to-top-anchor" />
      </div>
    </MuiThemeProvider>
  );
}

ReactDOM.render(
  <React.StrictMode>
    <BrowserRouter>
      <App />
    </BrowserRouter>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
