FROM node:15.2.1-alpine3.11

ARG CI

RUN mkdir -p /opt/app
RUN mkdir -p /opt/app/dist
WORKDIR /opt/app
ADD . /opt/app
ENV NODE_ENV production
RUN yarn set version berry
RUN yarn set version 3.2.0
RUN yarn install
RUN yarn build
CMD yarn run start:prod
