'use strict';
const nodemailer = require('nodemailer');
const express = require('express');
const router = express.Router();
const xss = require('xss');
const axios = require('axios');
require('dotenv').config();

router.get('/ping', function (req, res) {
  return res.status(200).json({ success: true, data: 'ping ok' });
});

// config smtp
const transport = {
  host: process.env.SMTP_HOST,
  port: process.env.SMTP_PORT,
  // To be clear, you should use port=465 + secure=true
  // or port=587 + secure=false, any other combinaison wont work
  secure: false, //process.env.SMTP_CONNECTION_MODE,
  auth: {
    user: process.env.SMTP_USERNAME,
    pass: process.env.SMTP_PASSWORD,
  },
};
const transporter = nodemailer.createTransport(transport);

transporter.verify((error, success) => {
  if (error) {    
    console.log('cannot connect to mailgun', error);
    console.log('transport', transport);
    console.log('if SMTP_PASSWORD === xxxx it is normal to fail, just fail in test')
  } else {
    console.log('connected to mailgun !');
  }
});

const RECAPTCHA_SERVER_KEY = '6Lckn-MZAAAAAAKFfMjzjeV58lp0o4KUhHzin8Tq';
// check si la valeur du captach est bonne sur le server de google
async function checkCaptcha(captchaValue) {
  const res = await axios.post(
    `https://www.google.com/recaptcha/api/siteverify?secret=${RECAPTCHA_SERVER_KEY}&response=${captchaValue}`,
    {},
    {
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8',
      },
    }
  );

  console.log('checkCaptcha', res.data.success);
  return res.data.success || false;
}

router.post('/sendmessage', async function (req, res) {
  try {
    const captchaValue = req.body.captchaValue;
    let resCaptcha = await checkCaptcha(captchaValue);
    if (!resCaptcha) {
      return res.status(200).json({ success: false, msg: 'Malheuresement une erreur de captcha est survenue.' });
    }

    const name = xss(req.body.name);
    const email = xss(req.body.email);
    let subjecttmp = 'Prise de contact à partir du site mnemotix.com';
    if (req.body.subject && req.body.subject.length > 0) {
      subjecttmp = req.body.subject;
    }

    const subject = xss(subjecttmp);
    const message = xss(req.body.message);

    // send mail with defined transport object
    let info = await transporter.sendMail({
      from: `"${name}" <${email}>`, // sender address
      replyTo: email,
      to: ['production', 'integration'].includes(process.env.NODE_ENV) ? 'contact@mnemotix.com' : 'ibrahimalain@hotmail.com', // can be a list , separated with ','
      subject,
      text: message, // plain text body
      html: `<p>${message}</p>`,
    });

    console.log('Message sent: %s', info.messageId);
    return res.status(200).json({ success: true, msg: 'Merci ! Votre message a été correctement envoyé.' });
  } catch (error) {
    console.error(error);
    return res.status(200).json({ success: false, msg: "Malheuresement une erreur est survenue lors de l'envoi de votre message." });
  }
});

router.get('/*', function (req, res) {
  console.log('unknow call');
});

module.exports = router;

/*
RECAPTCHA
public front  
6LdZouMZAAAAAAvi2vvDZwQaYa6cQ_Yju_Rp99k-

clé serveur
6LdZouMZAAAAAG7T55i8Gp80taeUuXHwfHMgZ1hV

*/
