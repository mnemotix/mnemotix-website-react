# mnemotix website v2 faite avec ReactJS


 
## Dossiers
 
le projet contient une api avec mailer et le code du front dans le dossier 'src' ( pas pu le renommer à create-react-app )


## Installation

```
yarn install (utiliser 'npm install --force' si il y a une erreur de dependances à cause de react-waypoint)
```

```
yarn start
```
> l'api demarre sur localhost:1234 ( le port est defini dans .env )


Elle sert l'API (merci captain obvious) mais aussi la prod du front qui ce trouvera dans /build



Pour mettre à jour la version de production du front qui ce trouvera dans /build

```
npm run build
```

attention pour le moment le port de l'api est le meme que celui du front , il est defini ici
```
// /src/utilities/axios.js
instance.defaults.baseURL = window.location.origin + '/api';
```

## Deploiement 

sortir une nouvelle buid: 


yarn version prerelease
yarn release

